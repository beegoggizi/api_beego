<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espace_offers', function($table) {
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->time('start_at')->nullable();
            $table->time('end_at')->nullable();
            $table->integer('minAge')->default(0);
            $table->integer('maxAge')->default(100);
            $table->integer('man')->default(1);
            $table->integer('woman')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espace_offers', function($table) {
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
            $table->dropColumn('minAge');
            $table->dropColumn('maxAge');
            $table->dropColumn('man');
            $table->dropColumn('woman');
        });
    }
}
