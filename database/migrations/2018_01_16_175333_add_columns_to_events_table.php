<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espace_events', function($table) {
            $table->date('end_date')->nullable();
            $table->string('lieu', 200)->nullable();
            $table->integer('inviter')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espace_events', function($table) {
            $table->dropColumn('end_date');
            $table->dropColumn('lieu');
            $table->dropColumn('inviter');
        });
    }
}
