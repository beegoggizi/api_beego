<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espaces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('isPremium')->nullable(); // for future update, predictions bitches !
            $table->string('address');
            $table->string('gouvernorat');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('country')->nullable();
            $table->text('description');
            $table->integer('picture')->unsigned();
            $table->integer('current')->unsigned();
            $table->float('note_finale')->default(2.5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espaces');
    }
}
