<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaceTracesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_traces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('espace_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('action'); // example : forcing color, adding event...
            $table->text('description')->nullable(); // example : the user X added an event with the name Y, at event->created_at
            $table->string('concerned_table')->nullable();
            $table->string('affected_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_traces');
    }
}
