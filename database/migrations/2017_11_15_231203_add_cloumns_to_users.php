<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('last_name');
            $table->integer('isAdmin')->default(0);
            $table->integer('sexe')->default(0); // 0 for man and 1 for woman and 2 for wabbéni
            $table->string('picture_path', 191)->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('gouvernorat')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('last_name');
            $table->dropColumn('isAdmin');
            $table->dropColumn('sexe');
            $table->dropColumn('picture_path');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('gouvernorat');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
    }
}
