<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 1/16/2018
 * Time: 7:34 PM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Espaces\IOfferRepository;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    protected $offers;
    protected $checks;

    public function __construct(IOfferRepository $offers, ICheckRepository $check)
    {
        $this->offers = $offers;
        $this->checks = $check;
    }

    public function get($id)
    {
        return ["status" => "success", "code" => "200", "data" => $this->offers->get($id)];
    }

    public function add(Request $request)
    {
        if(isset($request->name) && isset($request->description) && isset($request->espace_id))
            return ["status" => "success", "code" => "200", "data" => $this->offers->add($request)];
        else return ["status" => "error", "code" => "403", "msg" => "name, description and espace_id are required" ];
    }

    public function update(Request $request)
    {
        if(isset($request->name) && isset($request->description) && isset($request->espace_id))
            return ["status" => "success", "code" => "200", "data" => $this->offers->update($request)];
        else return ["status" => "error", "code" => "403", "msg" => "name, description and espace_id are required" ];
    }

    public function delete($id)
    {
        return ["status" => "success", "code" => "200", "data" => $this->offers->delete($id)];
    }

}