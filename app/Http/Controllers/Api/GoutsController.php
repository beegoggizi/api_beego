<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Comon\IGoutRepository;
use Illuminate\Http\Request;

class GoutsController extends Controller
{
    protected $checks;
    protected $gouts;

    public function __construct( IGoutRepository $gouts , ICheckRepository $check)
    {
        $this->gouts = $gouts;
        $this->checks = $check;
    }

    public function getAll()
    {
        return ['status' => 'success', 'code' => '200', 'gouts' => $this->gouts->getAll()];
    }

    public function getGoutById($id)
    {
        if($this->checks->goutExist($id))
            return ['status' => 'success', 'code' => '200', 'gout' => $this->gouts->getById($id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no gouts with the given id'];
    }

    public function getEspaceGouts($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'gouts' => $this->gouts->getEspaceGouts($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no espace with the given id'];
    }

    public function getUserGouts($user_id)
    {
        if($this->checks->userExist($user_id))
            return ['status' => 'success', 'code' => '200', 'gouts' => $this->gouts->getUserGouts($user_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no user with the given id'];
    }

    public function getEspacesFromGouts(Request $request)
    {
        return ['status' => 'success', 'code' => '200', 'espaces' => $this->gouts->getEspacesByGout(json_decode($request->gouts))];
    }

    public function addGoutToUser(Request $request)
    {
        if($this->checks->userExist($request->user_id) && $this->checks->goutExist($request->gout_id))
            return ['status' => 'success', 'code' => '200', 'new_gout' => $this->gouts->addGoutToUser($request)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no user or gout with the given id'];
    }

    public function deleteUserGout($gout_id, $user_id)
    {
        if($this->checks->userGoutExist($user_id, $gout_id))
            return ['status' => 'success', 'code' => '200', 'msg' => $this->gouts->deleteUserGout($gout_id, $user_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no gout with the given id'];
    }

    public function addGoutToEspace(Request $request)
    {
        if($this->checks->espaceExist($request->espace_id) && $this->checks->goutExist($request->gout_id))
            return ['status' => 'success', 'code' => '200', 'new_gout' => $this->gouts->addGoutToEspace($request)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no espace or gout with the given id'];
    }

    public function deleteEspaceGout($gout_id, $espace_id)
    {
        if($this->checks->espaceGoutExist($espace_id,$gout_id))
            return ['status' => 'success', 'code' => '200', 'msg' => $this->gouts->deleteEspaceGout($gout_id, $espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'no gout with the given id'];

    }

}
