<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/2/2017
 * Time: 8:59 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Comon\IFavoriteRepository;

class FavoritesController extends Controller
{
    protected $favorites;
    protected $checks;

    public function __construct(IFavoriteRepository $favorites, ICheckRepository $check)
    {
        $this->favorites = $favorites;
        $this->checks = $check;
    }

    public function create($user_id, $espace_id)
    {
        return $this->favorites->addFavorite($user_id, $espace_id);
    }

    public function delete($user_id, $espace_id)
    {
        return $this->favorites->deleteFavorite($user_id, $espace_id);
    }

    public function deleteById($id)
    {
        return $this->favorites->deleteFavoriteById($id);
    }

    public function getUserFavorites($user_id)
    {
        return $this->favorites->getUserFavorite($user_id);

    }

    public function getEspaceFavorites($espace_id)
    {
        return $this->favorites->getEspaceFavorite($espace_id);
    }

}