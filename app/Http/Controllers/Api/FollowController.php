<?php

namespace App\Http\Controllers\Api;

use App\GerantFollowRequest;
use App\Http\Controllers\controller;
use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Users\User;
use App\Http\Models\Users\UserFollow;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Comon\IEloquentFollowRepository;
use App\Http\Repositories\Notifications\IEloquentNotificationRepository;
use App\Http\services;

class FollowController extends controller
{
    protected $follow;
    protected $notification;
    protected $checks;

    public function __construct(IEloquentFollowRepository $follow,
                                IEloquentNotificationRepository $notification,
                                ICheckRepository $check)
    {
        $this->follow = $follow;
        $this->notification = $notification;
        $this->checks = $check;
    }

    public function follow($user, $espace)
    {
        $espaceExist = $this->checks->espaceExist($espace);
        $userExist = $this->checks->userExist($user);
        $trace = new services();

        if($espaceExist && $userExist )
        {
            $espace_name = Espace::find($espace)->name;

            $tryToFollow = $this->follow->follow($user, $espace, $this->checks->isUserAlreadyFollow($user, $espace));

            if($tryToFollow['Status'] == 'success')
                $this->notification->newfollowNotification($user, $espace);

            $trace->UserHistory($user,'Vous avez commencé à suivre '.$espace_name);
            return $tryToFollow;
        }
        else return ['status' => 'error', 'code' => '404',
                     'msg' => 'user or espace does not exist',
                    'userExist' => $userExist, 'espaceExist' => $espaceExist];

    }

    public function unfollow($user, $espace)
    {
        $espaceExist = $this->checks->espaceExist($espace);
        $userExist = $this->checks->userExist($user);
        $trace = new services();

        if($espaceExist && $userExist )
        {
            $espace_name = Espace::find($espace)->name;
            $trace->UserHistory($user,'Vous avez arrêter de suivre '.$espace_name);
            return $this->follow->unfollow($user, $espace);
        }

        else return ['status' => 'error', 'code' => '404',
                    'msg' => 'user or espace does not exist',
                    'userExist' => $userExist, 'espaceExist' => $espaceExist];
    }

    public function followers($espace)
    {
        $espaceExist = $this->checks->espaceExist($espace);
        if($espaceExist)
            return $this->follow->followers($espace);
        else return ['status' => 'error', 'code' => '404',
                    'msg' => 'Espace does not exist',
                    'espaceExist' => $espaceExist];
    }

    public function follows($user)
    { $userExist = $this->checks->userExist($user);
        if($userExist)
            return $this->follow->follows($user);
        else return ['status' => 'error', 'code' => '404',
        'msg' => 'User does not exist',
        'userExist' => $userExist];
    }

    public function userFollow($follower_id, $followed_id)
    {
        if($this->checks->userExist($follower_id) && $this->checks->userExist($followed_id))
        {      $trace = new services();
            if(!$this->checks->isUserAlreadyFollowUser($follower_id, $followed_id))
            {
                $concerned_user = User::find($followed_id);
                $user_name = $concerned_user->name.' '.$concerned_user->last_name;
                $trace->UserHistory($follower_id,'Vous avez commencé à suivre '.$user_name);
                return ['status' => 'success', 'code' => '200', 'result' => $this->follow->userFollow($follower_id, $followed_id)];
            }

            else return ['status' => 'error', 'code' => '308', 'msg' => 'The user already follow the given user id'];
        }
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }

    public function userUnfollow($follower_id, $followed_id)
    {
        if($this->checks->userExist($follower_id) && $this->checks->userExist($followed_id))
        {  $trace = new services();
            if($this->checks->isUserAlreadyFollowUser($follower_id, $followed_id))
            { $concerned_user = User::find($followed_id);
                $user_name = $concerned_user->name.' '.$concerned_user->last_name;
                $trace->UserHistory($follower_id,'Vous avez commencé à suivre '.$user_name);
                return ['status' => 'success', 'code' => '200', 'result' => $this->follow->userUnFollow($follower_id, $followed_id)];
            }

            else return ['status' => 'error', 'code' => '308', 'msg' => 'The user  does not follow the given user id'];
        }
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }

    public function getUserFollowers($user_id)
    {
        if($this->checks->userExist($user_id))
            return ['status' => 'success', 'code' => '200', 'followers' => $this->follow->getUserFollowers($user_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }

    public function search($espace_id, $query)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'data' => $this->follow->searchFollower($espace_id, $query)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'Espace does not exist'];
    }

    public function accept($id)
    {
        return ['status' => 'success', 'code' => '200', 'response' => $this->follow->accept($id)];
    }

    public function wait($id)
    {
        return ['status' => 'success', 'code' => '200', 'response' => $this->follow->wait($id)];
    }

    public function refuse($id)
    {
        return ['status' => 'success', 'code' => '200', 'response' => $this->follow->refuse($id)];
    }

    public function myWaitingList($user_id)
    {
        return ['status' => 'success', 'code' => '200', 'response' => $this->follow->myWaitingList($user_id)];
    }
    public function myRequests($user_id)
    {
        return ['status' => 'success', 'code' => '200', 'response' => $this->follow->myPendingRequests($user_id)];
    }

    public function getUsersSuggestions($espace_id)
    {
        $follower_ids = Follow::where('espace_id', '=', $espace_id)->pluck('user_id')->toArray();
        $friends = UserFollow::whereIn('user_id', $follower_ids)
                                ->whereNotIn('follower_id', $follower_ids)
                                ->pluck('follower_id')
                                ->toArray();
        $suggestions = User::whereIn('id', $friends)->paginate(10);
        return $suggestions;
    }

    public function sendRequestToUser($espace_id, $user_id)
    {
        $request = new GerantFollowRequest();
            $request->user_id = $user_id;
            $request->espace_id = $espace_id;
            $request->timestamps = false;
        $request->save();
        return ['status' => 'success', 'code' => '200', 'data' => $request];
    }

    public function getUserRequests($user_id)
    {
        $requests = GerantFollowRequest::where('user_id', '=', $user_id)->pluck('espace_id')->toArray();
        $espaces = Espace::select('name', 'country', 'address', 'note_finale', 'id')
                            ->whereIn('id', $requests)
                            ->paginate(10);
        return $espaces;
    }

    public function getEspaceRequests($espace_id)
    {
        $requests = GerantFollowRequest::where('espace_id', '=', $espace_id)->pluck('user_id')->toArray();
        $users = User::select('name', 'last_name', 'sexe', 'picture_path', 'id')
                    ->whereIn('id', $requests)
                    ->paginate(10);
        return $users;
    }

    public function acceptGerantRequest($user, $espace)
    {
        GerantFollowRequest::where('espace_id', '=', $espace)
                            ->where('user_id', '=', $user)
                            ->delete();
        return $this->follow($user, $espace);
    }

    public function refuseGerantRequest($user, $espace)
    {
        GerantFollowRequest::where('espace_id', '=', $espace)
            ->where('user_id', '=', $user)
            ->delete();
        return ['status' => 'success' ,'code' => '200', 'msg' => 'Request Refused'];
    }



}
