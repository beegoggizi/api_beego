<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Espaces\IPostRepository;
use Illuminate\Http\Request;

class PostsController extends controller
{
    protected $posts;
    protected $checks;

    public function __construct(IPostRepository $posts, ICheckRepository $check)
    {
        $this->posts = $posts;
        $this->checks = $check;
    }

    public function new(Request $request)
    {
        if($this->checks->espaceExist($request->espace_id))
            return ["status" => "success", "code" => "200", "post" => $this->posts->addPost($request)];
        else
            return ["status" => "error", "code" => "404", "msg" => "no espaces with the given id"];
    }

    public function update(Request $request)
    {
        if($this->checks->espaceExist($request->espace_id) && $this->checks->postExist($request->id))
            return ["status" => "success", "code" => "200", "post" => $this->posts->updatePost($request)];
        else
            return ["status" => "error", "code" => "404", "msg" => "no espaces/post with the given id"];
    }

    public function delete($id)
    {
        if( $this->checks->postExist($id))
        {
            $this->posts->deletePost($id);
            return ["status" => "success", "code" => "200", "msg" => "the post has been deleted" ];
        }
        else
            return ["status" => "error", "code" => "404", "msg" => "no post with the given id"];
    }

    public function getEspacePosts($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ["status" => "success", "code" => "200", "posts" => $this->posts->getEspacePosts($espace_id)];
        else
            return ["status" => "error", "code" => "404", "msg" => "no espaces with the given id"];
    }

    public function postsByUser($user_id)
    {
        return $this->posts->postsByUser($user_id);
    }
}