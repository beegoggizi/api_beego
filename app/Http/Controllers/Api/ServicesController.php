<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Models\Espaces\EspaceService;
use App\Http\Models\Espaces\services;


class ServicesController extends Controller
{
    public function getAll()
    {
        return ["status" => "success", "code" => "200", "data" => services::all()];
    }
    public function byespaceid($espace_id)
    {
        $ids = EspaceService::where('espace_id', '=', $espace_id)->pluck('service_id')->toArray();
        return ["status" => "success", "code" => "200", "data" => services::whereIn('id', $ids)->get()];
    }
    public function byserviceid($service_id)
    {
        return ["status" => "success", "code" => "200", "data" => services::where('id', $service_id)->first()];
    }
}
