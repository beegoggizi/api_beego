<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Models\Espaces\SpecialementPourVous;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class SpecialementPourVousController extends Controller
{
    public function add(Request $request)
    {
        if(isset($request->user_id) && isset($request->espace_id) && isset($request->description))
        {
            $spv = new SpecialementPourVous();
                $spv->user_id = $request->user_id;
                $spv->espace_id = $request->espace_id;
                $spv->description = $request->description;
                if(isset($request->image))
                {
                    if(strpos($request->image, 'base64') !== false )
                    {
                        $path     = url('pic/events');
                        $fileName = "event-".$spv->id."-".time().".png";
                        $fullUrl  = $path."/".$fileName;
                        $spv->image = $fullUrl;
                        Image::make($request->image)->save('pic/events/'.$fileName);
                    }
                }
             $spv->save();
                return ["status" => "success", "code" => "200", "data" => $spv];
        }
        return ["status" => "error", "code" => "302", "msg" => "Missing one of those required fields : user_id, espace_id, description"];
    }

    public function update(Request $request)
    {
        if(isset($request->id))
        {
           if($spv = SpecialementPourVous::find($request->id))
           {
               if(isset($request->user_id)) $spv->user_id = $request->user_id;
               if(isset($request->espace_id)) $spv->espace_id = $request->espace_id;
               if(isset($request->description)) $spv->description = $request->description;
               if(isset($request->image))
               {
                   if(strpos($request->image, 'base64') !== false )
                   {
                       $path     = url('pic/events');
                       $fileName = "event-".$spv->id."-".time().".png";
                       $fullUrl  = $path."/".$fileName;
                       $spv->image = $fullUrl;
                       Image::make($request->image)->save('pic/events/'.$fileName);
                   }
               }
               $spv->save();
               return ["status" => "success", "code" => "200", "data" => $spv];
           }

            return ["status" => "error", "code" => "302", "msg" => "No row with the given id"];
        }
        return ["status" => "error", "code" => "302", "msg" => "The field id is required (the id of the specialement_pour_vous"];
    }
    public function getByUserId($user_id)
    {
        return ["status" => "success", "code" => "200", "data" => SpecialementPourVous::where('user_id', '=', $user_id)->get()];
    }
    public function getByEspaceId($espace_id)
    {
        return ["status" => "success", "code" => "200", "data" => SpecialementPourVous::where('espace_id', '=', $espace_id)->get()];
    }
    public function delete($user_id, $espace_id)
    {
        $spv = SpecialementPourVous::where('user_id', '=', $user_id)->where('espace_id', '=', $espace_id)->first();
        $spv->delete();
        return ["status" => "success", "code" => "200", "msg" => "Success ! the row has been deleted. "];
    }

}
