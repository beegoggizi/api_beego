<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Repositories\Notifications\IEloquentNotificationRepository;


class NotificationController extends Controller
{
    protected $notifications;

    public function __construct(IEloquentNotificationRepository $notification)
    {
       $this->notifications = $notification;
    }

    public function getUserNotifications($user)
    {
        return $this->notifications->getUserNotifications($user);
    }

    public function getEspaceNotifications($espace)
    {
        return $this->notifications->getEspaceNotifications($espace);
    }
}
