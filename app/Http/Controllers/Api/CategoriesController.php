<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Models\Espaces\Categories;
use App\Http\Models\Espaces\EspaceCategories;
use App\Http\Models\Espaces\SousCategories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function add(Request $request)
    {
        if(isset($request->name))
        {
            $categorie = new Categories();
                $categorie->name = $request->name;
                if(isset($request->description)) $categorie->description = $request->description;
            $categorie->save();
            return ["status" => "success", "code" => "200", "data" => $categorie];
        }
        else return ["status" => "error", "code" => "302", "msg" => "The name field is required, please specify the name of the categorie"];
    }

    public function update(Request $request)
    {
        if(isset($request->id))
        {
            $categorie = Categories::find($request->id);
                $categorie->name = $request->name;
                if(isset($request->description)) $categorie->description = $request->description;
             $categorie->save();
             return ["status" => "success", "code" => "200", "data" => $categorie];
        }
        else return ["status" => "error", "code" => "302", "msg" => "The id of the category is required"];
    }

    public function delete($id)
    {
        Categories::find($id)->delete();
        return ["status" => "success", "code" => "200", "msg" => "the category has been deleted"];
    }

    public function addSousCategory(Request $request)
    {
        if(isset($request->category_id) && isset($request->name))
        {
            $s_category = new SousCategories();
                $s_category->category_id = $request->category_id;
                $s_category->name = $request->name;
            $s_category->save();
            return ["status" => "success", "code" => "200", "data" => $s_category];
        }
        else return ["status" => "error", "code" => "302", "msg" => "Missing one of those fields: category_id or name (the name of the sous_category)"];
    }

    public function deleteSousCategory($id)
    {
        SousCategories::find($id)->delete();
        return ["status" => "success", "code" => "200", "msg" => "the sous_category has been deleted"];
    }

    public function addSousCategoryToEspace($espace_id, $sous_category)
    {
        $EspaceCategory = new EspaceCategories();
            $EspaceCategory->espace_id = $espace_id;
            $EspaceCategory->sous_category = $sous_category;
        $EspaceCategory->save();
        return ["status" => "success", "code" => "200", "data" => $EspaceCategory];
    }

    public function getEspaceCategories($espace_id)
    {
        $ids = EspaceCategories::where('espace_id', '=', $espace_id)->pluck('sous_category')->toArray();
        $categories = SousCategories::whereIn('id', $ids)->get();
        return ["status" => "success", "code" => "200", "data" => $categories];
    }

    public function getSousCategory($id)
    {
        return ["status" => "success", "code" => "200", "data" => SousCategories::where('category_id', '=', $id)->get()];
    }
}
