<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/2/2017
 * Time: 12:12 PM
 */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;


use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Comon\IFeedbackRepository;
use Illuminate\Http\Request;

class FeedbackController extends controller
{
    protected $feedbacks;
    protected $checks;

    public function __construct(IFeedbackRepository $feedbacks, ICheckRepository $check)
    {
        $this->feedbacks = $feedbacks;
        $this->checks = $check;
    }


    public function getEspaceFeedbacks($espace)
    {
        return $this->feedbacks->espaceFeedbacks($espace);
    }

    public function getUserFeedbacks($user)
    {
        return $this->feedbacks->userFeedback($user);
    }

    public function deleteFeedback($user, $espace)
    {
        return $this->feedbacks->deleteFeedback($user, $espace);
    }

    public function deleteFeedbackById($id)
    {
        return $this->feedbacks->deleteFeedbackById($id);
    }

    public function addFeedback(Request $request)
    {
        return $this->feedbacks->addFeedback($request->user_id, $request->espace_id, $request->comment, $request->note, $this->checks->isUserAlreadyFeedbacked($request->user_id, $request->espace_id));
    }

    public function updateFeedback(Request $request)
    {
        return $this->feedbacks->updateFeedback($request->user_id, $request->espace_id, $request->comment, $request->note);
    }

    public function updateFeedbackById(Request $request)
    {
        return $this->feedbacks->updateFeedbackById($request->id, $request->comment, $request->note);
    }

    public function like($id, $like)
    {
        return $this->feedbacks->like($id, $like);
    }
}