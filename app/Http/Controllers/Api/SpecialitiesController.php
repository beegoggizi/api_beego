<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Espaces\ISpecialityRepository;
use Illuminate\Http\Request;

class SpecialitiesController extends Controller
{
    protected $specialities;
    protected $checks;

    public function __construct(ISpecialityRepository $specialities, ICheckRepository $check)
    {
        $this->specialities = $specialities;
        $this->checks = $check;
    }

    public function get($id)
    {
        return ["status" => "success", "code" => "200", "data" => $this->specialities->getEspaceSpecialities($id)];
    }

    public function add(Request $request)
    {   
        $s = \GuzzleHttp\json_decode($request->specialities);
        foreach ($s as $speciality) {
            $this->specialities->add($speciality);
        }
            return ["status" => "success", "code" => "200"];
    }

    public function update(Request $request)
    {
        if(isset($request->name) && isset($request->description) && isset($request->espace_id))
            return ["status" => "success", "code" => "200", "data" => $this->specialities->update($request)];
        else return ["status" => "error", "code" => "403", "msg" => "name, description and espace_id are required" ];
    }

    public function delete($id)
    {
        return ["status" => "success", "code" => "200", "data" => $this->specialities->delete($id)];
    }

    public function getUserSpecialities($user_id)
    {
        return ["status" => "success", "code" => "200", "data" => $this->specialities->getUserSpecialities($user_id) ];
    }
}
