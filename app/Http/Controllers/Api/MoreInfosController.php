<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Models\Espaces\MoreInfos;
use Illuminate\Http\Request;

class MoreInfosController extends Controller
{
    public function add(Request $request)
    {
        foreach ($request as $item)
        {
            if(isset($item->espace_id) && isset($item->name) && isset($item->description))
            {
                $mi = new MoreInfos();
                    $mi->espace_id = $item->espace_id;
                    $mi->name = $item->name;
                    $mi->description = $item->description;
                $mi->save();
                if(isset($item->picture))
                {   $image = $item->pictures;
                    if(strpos($image, 'base64') !== false )
                    {
                        $path     = url('pic/moreinfos');
                        $fileName = "infos-".$mi->id."-".time().".png";
                        $fullUrl  = $path."/".$fileName;
                        $mi->picture           = $fullUrl;
                        Image::make($image)->save('pic/moreinfos/'.$fileName);
                        $mi->save();
                    }
                }
            }
        }
            return ["status" => "success", "code" => "200", "msg" => "Data has been added"];
    }

    public function update(Request $request)
    {
        if(isset($request->id))
        {
           if($mi = MoreInfos::find($request->id))
           {
                   if(isset($request->espace_id)) $mi->espace_id = $request->espace_id;
                   if(isset($request->name)) $mi->name = $request->name;
                   if(isset($request->description)) $mi->description = $request->description;
                   if(isset($request->picture))
                   {   $image = $request->picture;
                       if(strpos($image, 'base64') !== false )
                       {
                           $path     = url('pic/moreinfos');
                           $fileName = "infos-".$mi->id."-".time().".png";
                           $fullUrl  = $path."/".$fileName;
                           $mi->picture           = $fullUrl;
                           Image::make($image)->save('pic/moreinfos/'.$fileName);
                       }
                   }
               $mi->save();
               return ["status" => "success", "code" => "200", "data" => $mi];
           }
            return ["status" => "error", "code" => "302", "msg" => "No row with the given id"];
        }
        return ["status" => "error", "code" => "302", "msg" => "The id of the row is required"];
    }

    public function delete($id)
    {
        if($test = MoreInfos::find($id))
        {
            $test->delete();
            return ["status" => "success", "code" => "200", "msg" => "Success ! the info has been deleted"];
        }
        return ["status" => "error", "code" => "302", "msg" => "No row with the given id"];
    }

    public function get($espace_id)
    {
        return ["status" => "success", "code" => "200", "data" => MoreInfos::where('espace_id', '=', $espace_id)->get()];
    }
}
