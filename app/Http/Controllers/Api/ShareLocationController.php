<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Models\Users\ShareLocation;

class ShareLocationController extends Controller
{
    public function invite($invited, $inviter)
    {
        $share = new ShareLocation();
            $share->user_id = $invited;
            $share->share_with_id = $inviter;
         $share->save();
         return ["status" => "success", "code" => "200", "data" => $share];
    }

    public function accept($invitation_id, $long, $lat)
    {
        $share = ShareLocation::find($invitation_id);
            $share->long = $long;
            $share->lat = $lat;
            $share->status = 1;
        $share->save();
        return ["status" => "success", "code" => "200", "data" => $share];
    }

    public function reject($invitation_id)
    {
        if($share = ShareLocation::find($invitation_id))
        {
            $share->delete();
            return ["status" => "success", "code" => "200", "msg" => "The invitation has been rejected"];
        }
        else return ["status" => "error", "code" => "404", "msg" => "No invitations with the given id"];
    }

    public function getPosition($user_id, $shared_with)
    {
        if($test = ShareLocation::with('user:id,name,last_name,picture_path', 'user_shared_with:id,name,last_name,picture_path')
                                ->where('user_id', '=', $user_id)
                                ->where('share_with_id', '=', $shared_with)
                                ->first())
            return ["status" => "success", "code" => "200", "data" => $test];
        return  ["status" => "error", "code" => "404", "msg" => "No shares with the given couple"];
    }

    public function myInvits($user_id)
    {
        return ["status" => "success", "code" => "200", "data" => ShareLocation::with('user:id,name,last_name,picture_path', 'user_shared_with:id,name,last_name,picture_path')
                                                                                ->where('share_with_id', '=', $user_id)
                                                                                ->where('status', '=', 0)->get()];
    }

    public function peopleSharingWithMe($user_id)
    {
        return ["status" => "success", "code" => "200", "data" => ShareLocation::with('user:id,name,last_name,picture_path', 'user_shared_with:id,name,last_name,picture_path')
                                                                                ->where('share_with_id', '=', $user_id)
                                                                                ->where('status', '=', 1)->get()];
    }

    public function myShares($user_id)
    {
        return ["status" => "success", "code" => "200", "data" => ShareLocation::with('user:id,name,last_name,picture_path', 'user_shared_with:id,name,last_name,picture_path')
                                                                                ->where('user_id', '=', $user_id)
                                                                                ->where('status', '=', 1)->get()];
    }

/*
    if($test = ShareLocation::where('user_id', '=', $user_id)->where('share_with_id', '=', $shared_with)->first())
        {
            $user = User::find($user_id);
            $shared_with_user = User::find($shared_with);
            $test->user_picture = $user->picture_path;
            $test->user_name = $user->name.' '.$user->last_name;
            $test->shared_with_picture = $shared_with_user->picture_path;
            $test->shared_with_name =  $shared_with_user->name.' '.$shared_with_user->last_name;
            return ["status" => "success", "code" => "200", "data" => $test];
        }

 */
}
