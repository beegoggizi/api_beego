<?php
namespace App\Http\Controllers\Api;


use App\GerantReport;
use App\Http\Controllers\controller;
use Illuminate\Http\Request;


class ReportsController extends controller
{
    public function reportUser(Request $request)
    {
        $report = new GerantReport();
            $report->user_id = $request->user_id;
            $report->espace_id = $request->espace_id;
            $report->description = $request->description;
        $report->save();

        return ['status' => 'success', 'code' => '200', 'msg' => 'Merci ! nous vous répondrons dans les plus bref délais'];
    }

    public function getEspaceReports($espace_id)
    {
        $reports = GerantReport::with('user')->paginate(10);
        return $reports;
    }

    public function deleteReport($id)
    {
        GerantReport::find($id)->delete();
        return ['status' => 'success', 'code' => '200', 'msg' => 'Le signalement est supprimé'];
    }

    public function updateReport(Request $request)
    {
        $report = GerantReport::find($request->id);
            $report->user_id = $request->user_id;
            $report->espace_id = $request->espace_id;
            $report->description = $request->description;
        $report->save();
        return ['status' => 'success', 'code' => '200', 'msg' => 'Le signalement a été mis à jour', 'data' => $report];

    }
}