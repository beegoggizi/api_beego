<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Espaces\IEventRepository;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    protected $events;
    protected $checks;

    public function __construct(IEventRepository $events, ICheckRepository $check)
    {
        $this->events = $events;
        $this->checks = $check;
    }

    public function getAll()
    {
        return ['status' => 'success', 'code' => '200', 'Events' => $this->events->getAll()];
    }


    public function new(Request $event)
    {
        $result = $this->events->addEvent($event);
        if($result != null)
            return ["status" => "success", "code" => "200", "event" => $result];
        else
            return ["status" => "error", "code" => "400", "msg" => "an error has occured while trying to add the event"];
    }

    public function update(Request $event)
    {
        if($this->checks->eventExist($event->id))
        {
            $result = $this->events->updateEvent($event);
            if($result != null)
                return ["status" => "success", "code" => "200", "event" => $result];
            else
                return ["status" => "error", "code" => "400", "msg" => "an error has occured while trying to update the event"];
        }
        else return ["status" => "error", "code" => "408", "msg" => "The given event Id does not exist"];

    }

    public function delete($id)
    {
        if($this->checks->eventExist($id))
        {
            $this->events->delete($id);
            return ["status" => "success", "code" => "200", "msg" => "event deleted"];
        }
        else return ["status" => "error", "code" => "408", "msg" => "The given event Id does not exist"];
    }

    public function getEspaceEvents($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ["status" => "success", "code" => "200", "events" => $this->events->getEspaceEvent($espace_id)];
        else return ["status" => "error", "code" => "408", "msg" => "The given espace Id does not exist"];
    }

    public function userInterrested($user_id, $event_id)
    {
        if($this->checks->userExist($user_id) && $this->checks->eventExist($event_id))
        {
            if(!$this->checks->userAlreadyInterrestedEvent($user_id, $event_id))
                return ["status" => "success", "code" => "200", "events" => $this->events->userInterrested($user_id, $event_id)];
            else return ["status" => "error", "code" => "408", "msg" => "The user is already interrested in that event"];
        }

        else return ["status" => "error", "code" => "408", "msg" => "The given event or user Id does not exist"];
    }
}
