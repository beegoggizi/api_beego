<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Models\Espaces\Espace_offer;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Espaces\IEspaceRepository;
use Illuminate\Http\Request;
use App\Http\Models\Espaces\Espace_event;

class EspaceController extends Controller
{
    protected $espaces;
    protected $checks;

    public function __construct(IEspaceRepository $espace, ICheckRepository $check)
    {
        $this->espaces = $espace;
        $this->checks = $check;
    }

    public function getEspaceById($espace)
    {
        $espaceExist = $this->checks->espaceExist($espace);
        if($espaceExist)
            return ['status' => 'success', 'code' => '200', 'espace' => $this->espaces->getEspaceById($espace)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function getEspaces($dist, $user_lng, $user_lat)
    {
        return ['status' => 'success', 'code' => '200', 'espace' => $this->espaces->getEspaces($dist, $user_lng, $user_lat)];
    }

    public function getAllEspaces()
    {
        return ['status' => 'success', 'code' => '200', 'espace' => $this->espaces->getAllEspaces()];
    }


    public function updateEspace(Request $request)
    {
        if($this->checks->espaceExist($request->id))
            return ['status' => 'success', 'code' => '200', 'espace' => $this->espaces->update($request)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function forceState(Request $request)
    { if($this->checks->espaceExist($request->espace_id))
        return ['status' => 'success', 'code' => '200', 'state' => $this->espaces->forceEspaceState($request->espace_id, $request->color)];
    else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function unforceState($espace_id)
    { if($this->checks->espaceExist($espace_id))
        return ['status' => 'success', 'code' => '200', 'state' => $this->espaces->unforceEspaceState($espace_id)];
    else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function getSuggestions()
    {
        return ['status' => 'success', 'code' => '200', 'suggestions' => $this->espaces->getSuggestions()];
    }

    public function getSuggestionByEspace($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'suggestions' => $this->espaces->getSuggestionByEspaceId($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function CountSuggestionEspaces($id)
    {
        return ['status' => 'success', 'code' => '200', 'count' => $this->espaces->countSuggestions($id)];
    }

    public function getSuggestionEspace($id)
    {
        return ['status' => 'success', 'code' => '200', 'count' => $this->espaces->getSuggestionEspaces($id)];
    }

    public function getOffersByEspace($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'promotions' => $this->espaces->getOffersByEspaceId($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function search($name)
    {
        return ['status' => 'success', 'code' => '200', 'results' => $this->espaces->searchByName($name)];
    }

    public function getEspaceNews($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return  ['status' => 'success', 'code' => '200', 'news' => $this->espaces->getEspaceNews($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function getFollowersCount($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'count' => $this->espaces->getFollowersCount($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function espaceActivities($id, $date)
    {
        $events = Espace_event::Where('event_date', '>=', $date)
                        ->where('espace_id', '=', $id)
                        ->get();
        $promotions = Espace_offer::where('espace_id', '=', $id)
                                ->where('start_date', '>=', $date)
                                ->get();
        return ["status" => "success", "code" => "200", "events" => $events, "promotions" => $promotions];
    }

    public function changeHoraire(Request $request)
    {
        if($this->checks->espaceExist($request->espace_id))
            return ['status' => 'success', 'code' => '200', 'count' => $this->espaces->changeTime($request->horaires, $request->espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id '.$request->espace_id];
    }

    public function getHoraire($espace_id)
    {
        if($this->checks->espaceExist($espace_id))
            return ['status' => 'success', 'code' => '200', 'count' => $this->espaces->getHoraires($espace_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id '.$espace_id];
    }

}
