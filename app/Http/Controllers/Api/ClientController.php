<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\controller;
use App\Http\Models\Espaces\Espace;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Notifications\IEloquentNotificationRepository;
use App\Http\Repositories\Users\IUserRepository;
use App\Http\services;
use App\UserHistory;
use Illuminate\Http\Request;

class ClientController extends Controller
{
   protected $users;
    protected $checks;
    protected $notifications;

    public function __construct( IUserRepository $user , ICheckRepository $check, IEloquentNotificationRepository $notification)
    {
        $this->users = $user;
        $this->checks = $check;
        $this->notifications = $notification;
    }

    public function getUser($id)
    {
        if($this->checks->userExist($id))
            return ['status' => 'success', 'code' => '200', 'user' => $this->users->getUser($id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }

    public function getAll()
    {
        return ['status' => 'success', 'code' => '200', 'user' => $this->users->getAll()];
    }

    public function addUser(Request $request)
    {
        return ['status' => 'success', 'code' => '200', 'user' => $this->users->addUser($request)];
    }
    public function updatePosition(Request $request)
    {
        if($this->checks->userExist($request->user_id))
            return ['status' => 'success', 'code' => '200', 'user' => $this->users->updatePosition($request->longitude, $request->latitude, $request->user_id)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }
    public  function updateUser(Request $request)
    {
        if($this->checks->userExist($request->id))
            return ['status' => 'success', 'code' => '200', 'user' => $this->users->updateUser($request)];
        else return ['status' => 'error', 'code' => '404', 'msg' => 'User does not exist'];
    }

    public function createProgram(Request $request)
    {
        $program = $this->users->createProgram($request);
        $this->notifications->newProgramNotification($program);
        $trace = new services();
        $espace_name = Espace::find($request->espace_id)->name;
        $trace->UserHistory($request->user_id, 'Vous avez créer un nouveau programme dans '.$espace_name.' pour le '.$request->date_time);
        return ['status' => 'success', 'code' => '200', 'program' => $program];
    }

    public function inviteToProgram(Request $request)
    {
        return ['status' => 'success', 'code' => '200', 'program' => $this->users->inviteToProgram($request->program_id, $request->invites)];
    }

    public function getUserPrograms($user_id)
    {
        return ['status' => 'success', 'code' => '200', 'programs' => $this->users->getProgramByUserId($user_id)];
    }

    public function programsImInvitedTo($user_id)
    {
        return ['status' => 'success', 'code' => '200', 'programs' => $this->users->programsImInvitedTo($user_id)];
    }

    public function search($query)
    {
        return ['status' => 'success', 'code' => '200', 'results' => $this->users->searchUser($query)];
    }

    public function changePassword(Request $request)
    {
        return $this->users->changePassword($request);
    }

    public function getUserHistory($user)
    {
        return ['status' => 'success', 'code' => '200', 'results' => UserHistory::where('user_id', '=', $user)->get()];
    }

    public function deleteHistory($user)
    {
        UserHistory::where('user_id', '=', $user)->delete();
        return ['status' => 'success', 'code' => '200'];
    }
}
