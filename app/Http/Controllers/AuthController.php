<?php

namespace App\Http\Controllers;

use App\Http\Models\Espaces\Espace;
use App\Http\Models\Users\Role;
use App\Http\services;
use App\Route;
use App\Legislature;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller {

    public function authenticate(\Illuminate\Http\Request $request) {
        $credentials = $request->only('email', 'password'); // grab credentials from the request
        $history = new services();
        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return ['status' => 'error', 'code' => '401', 'msg' => 'invalid_credentials'];
            }
        } catch (JWTException $e) {
            return ['status' => 'error', 'code' => '500', 'msg' => 'could_not_create_token']; // something went wrong whilst attempting to encode the token
        }

        $user = User::where('email', '=', $request->email)
                        ->first();
        if($request->device_id) //check if there is a device id
        {
            $user->device_id = $request->device_id;  //if yes update it
            $user->save();
        }

        $role = Role::where('user_id', '=', $user->id)->where('role_name', '=', 'gerant')->orWhere('role_name', '=', 'sous_gerant')->first();
        if(!$role)
                $role = "user";
        else
        {
            $espace = Espace::find($role->espace_id);
            $history->EspaceHistory($espace->id, 'Vous vous êtes connecté à votre compte');
            return response()->json(['status' => 'success', 'code' => '200',  'role' => $role->role_name , 'espace' => $espace, 'token' => "Bearer $token"]);

        }
            $history->UserHistory($user->id, 'Vous vous êtes connecté à votre compte');
            return response()->json(['status' => 'success', 'code' => '200', 'role' => $role , 'user' => $user, 'token' => "Bearer $token"]);

    }
}