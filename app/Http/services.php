<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/13/2017
 * Time: 9:33 PM
 */

namespace App\Http;


use App\EspaceHistory;
use App\Http\Models\Logs\Espace_trace;
use App\Http\Models\Users\Trace_user;
use App\UserHistory;
use Carbon\Carbon;

class services
{
    public function addUserTrace($user_id, $action, $description, $table_name, $table_id)
    {
        $new = new Trace_user();
            $new->user_id = $user_id;
            $new->type = $action;
            $new->action_description = $description;
            $new->table_name = $table_name;
            $new->table_id = $table_id;
        $new->save();

        return $new;
    }

    public function addEspaceTrace($espace_id, $user_id, $action, $description, $concerned_table, $affected_id)
    {
        $new = new Espace_trace();
            $new->espace_id = $espace_id;
            $new->user_id = $user_id;
            $new->action = $action;
            $new->description = $description;
            $new->concerned_table = $concerned_table;
            $new->affected_id = $affected_id;
        $new->save();

        return $new;
    }

    public function UserHistory($user_id, $description)
    {
        $new = new UserHistory();
            $new->user_id = $user_id;
            $new->description = $description;
         $new->save();
    }

    public function EspaceHistory($espace_id, $description)
    {
        $new = new EspaceHistory();
            $new->espace_id = $espace_id;
            $new->description = $description;
        $new->save();
    }

    function pushNotification($message, $id) {


        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'registration_ids' =>$id
            ,
            'notification' => array (
                "title" => "Beego",
                "score"=> "5x1",
                "time"=> Carbon::now(),
                "body"=> $message,
                "image"=>"https://www.appcoda.com/wp-content/uploads/2016/11/firebase_logo_shot.png",
                "content-available"=>"true",
                "force-start"=>"1",
                "click_action"=>"FCM_PLUGIN_ACTIVITY",
                "sound"=> "runner.mp3"
            ),
            'data' => array (
                "sendername" => "Beego",
                "message" => $message,
                "body"=> "Une description pour voir ce que c'est ce champ",
                "title" => "Beego"
            )
        );

        $fields = json_encode ( $fields );
        $headers = array (
            'Authorization: key=' . "AAAAjtFsXYQ:APA91bETxkPnh_VMcazIF9agq2sWBbHvznCYdY1tRn1gyyANRebHQ-lLiQuN3PhUsrRZ0kNEhKUtaqdTNUmk6dS9pDB9jMcWvnYPH_DDepdiRvuVpQ-poZpbvYFQyD2tMbsn-ZWgV5Qm",
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );

        curl_close ( $ch );
        return $result;
    }
}