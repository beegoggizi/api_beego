<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function espace()
    {
        return $this->belongsTo('App\Http\Models\Espaces\Espace', 'espace_id');
    }
}
