<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace_config extends Model
{
    protected $dates = ['created_at', 'updated_at'];
}
