<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace extends Model
{
    protected $fillable = ['address', 'gouvernorat', 'longitude', 'latitude', 'description', 'country', 'note_finale', 'current'];
    protected $dates = ['created_at', 'updated_at'];

    public function followers()
    {
        return $this->hasMany('App\Http\Models\Comon\Follow', 'espace_id', 'id');
    }

    public function pictures()
    {
        return $this->hasMany('App\Http\Models\Pictures\Espace_picture', 'espace_id', 'id');
    }

    public function offers ()
    {
        return $this->hasMany('App\Http\Models\Espaces\Espace_offer', 'espace_id', 'id');
    }

    public function gouts()
    {
        return $this->belongsToMany('App\Http\Models\Espaces\espace_gout', 'App\Http\Models\Comon\Gout', 'espace_id');
    }

    public function horaires()
    {
        return $this->hasMany('App\Http\Models\Espaces\Espace_horaires', 'espace_id', 'id');
    }

    public function specialities()
    {
        return $this->hasMany('App\Http\Models\Espaces\Espace_speciality', 'espace_id', 'id');
    }

    public function events()
    {
        return $this->hasMany('App\Http\Models\Espaces\Espace_event', 'espace_id', 'id');
    }

    public function visits()
    {
        return $this->hasMany('App\Http\Models\Users\UserVisitEspace', 'espace_id', 'id');
    }

    public function trace()
    {
        return $this->hasMany('App\Http\Models\Logs\Espace_trace', 'espace_id', 'id');
    }

    public function config()
    {
        return $this->hasOne('App\Http\Models\Espaces\Espace_config', 'espace_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany('App\Http\Models\Espaces\EspacePosts', 'espace_id', 'id');
    }

    public function moreInfos()
    {
        return $this->hasMany('App\Http\Models\Espaces\MoreInfos', 'espace_id', 'id');
    }


}
