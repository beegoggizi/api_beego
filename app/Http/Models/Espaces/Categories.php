<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function SousCategories()
    {
        return $this->hasMany("App\Http\Models\Espaces\SousCategories", "category_id", "id");
    }
}
