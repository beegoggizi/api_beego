<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class EspacePosts extends Model
{
    public function pictures()
    {
        return $this->hasMany('App\Http\Models\Pictures\PostsPicture', 'event_id', 'id');
    }
}
