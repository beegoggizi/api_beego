<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace_speciality extends Model
{
    protected $table = 'espace_specailities';
    protected $fillable = ["espace_id", "name", "description"];
    protected $dates = ["created_at", "updated_at"];

    public function pictures()
    {
        return $this->hasMany('App\Http\Models\Espaces\SpecialityPicture', 'speciality_id', 'id');
    }
}
