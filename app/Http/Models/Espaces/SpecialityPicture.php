<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class SpecialityPicture extends Model
{
    //
    protected $dates = ["created_at", "updated_at"];
}
