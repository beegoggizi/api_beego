<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class espace_gout extends Model
{
    protected $fillable = ["espace_id", "gout_id"];
    protected $dates = ['created_at', 'updated_at'];
}
