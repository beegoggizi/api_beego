<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace_horaires extends Model
{
    protected $fillable = ["espace_id", "day", "open_at", "close_at"];
    protected $dates = ['created_at', 'updated_at'];
}
