<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace_event extends Model
{
    protected $fillable = ["title", "description", "tags", "event_date", "start_at", "end_at", "espace_id", "user_id"];
    protected $dates = ['created_at', 'updated_at'];

    public function pictures()
    {
        $this->hasMany('App\Http\Models\Pictures\Event_picture', 'event_id', 'id');
    }
}
