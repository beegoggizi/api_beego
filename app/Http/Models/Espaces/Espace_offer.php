<?php

namespace App\Http\Models\Espaces;

use Illuminate\Database\Eloquent\Model;

class Espace_offer extends Model
{
    protected $fillable = ["espace_id", "name", "description"];
    protected $dates = ["created_at", "updated_at"];

    public function pictures()
    {
        return $this->hasMany('App\Http\Models\Pictures\Offer_picture', 'offer_id', 'id');
    }

    public function espace()
    {
        return $this->belongsTo('App\Http\Models\Espaces\Espace', 'espace_id');
    }
}
