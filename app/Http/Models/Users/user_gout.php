<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class user_gout extends Model
{
    protected $fillable = ["user_id", "gout_id"];
    protected $dates = ["created_at", "updated_at"];
}
