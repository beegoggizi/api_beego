<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class ShareLocation extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Http\Models\Users\User', 'user_id');

    }

    public function user_shared_with()
    {
       return $this->belongsTo('App\Http\Models\Users\User', 'share_with_id', 'id');
    }
}
