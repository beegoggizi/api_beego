<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ["role_name", "user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];

}
