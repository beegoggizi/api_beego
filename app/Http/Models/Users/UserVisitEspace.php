<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserVisitEspace extends Model
{
    protected $fillable = ["user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];
}
