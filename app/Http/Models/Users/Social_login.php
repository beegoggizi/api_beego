<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Social_login extends Model
{
    protected $fillable = ["user_id", "api_userId", "token", "type"];
    protected $dates = ["created_at", "updated_at"];
}
