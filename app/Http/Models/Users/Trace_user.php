<?php

namespace App\Http\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Trace_user extends Model
{
    protected $fillable = ["user_id", "type", "action_description", "table_name", "table_id"];
    protected $dates = ["created_at", "updated_at"];
}
