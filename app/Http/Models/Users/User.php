<?php

namespace App\Http\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_name', 'sex', 'phone', 'address', 'longitude', 'latitude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ["created_at", "updated_at"];

    public function follow()
    {
        return $this->hasMany('App\Http\Models\Comon\Follow', 'user_id', 'id');
    }

    public function socialLogins()
    {
        $this->hasMany('App\Http\Models\Users\Social_login', 'user_id', 'id');
    }

    public function userTrace()
    {
        $this->hasMany('App\Http\Models\Users\Trace_user', 'user_id', 'id');
    }

    public function gouts()
    {
        $this->hasMany('App\Http\Models\Users\user_gout', 'user_id', 'id');
    }

    public function visitedEspaces()
    {
        $this->hasMany('App\Http\Models\Users\UserVisitEspace', 'user_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Http\Models\Notifications\Notification', 'user_id', 'id');
    }
}
