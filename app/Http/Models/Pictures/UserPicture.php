<?php

namespace App\Http\Models\Pictures;

use Illuminate\Database\Eloquent\Model;

class UserPicture extends Model
{
    protected $fillable = ["user_id", "file_name", "path"];
    protected $dates = ["created_at", "updated_at"];
}
