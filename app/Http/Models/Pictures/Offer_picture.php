<?php

namespace App\Http\Models\Pictures;

use Illuminate\Database\Eloquent\Model;

class Offer_picture extends Model
{
    protected $fillable = ["offer_id", "file_name", "path", "priority"];
    protected $dates = ["created_at", "updated_at"];
}
