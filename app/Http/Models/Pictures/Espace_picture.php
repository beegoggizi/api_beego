<?php

namespace App\Http\Models\Pictures;

use Illuminate\Database\Eloquent\Model;

class Espace_picture extends Model
{
    protected $filliable = ["espace_id", "file_name", "path", "priority"];
    protected $dates = ["created_at", "updated_at"];
}
