<?php

namespace App\Http\Models\Pictures;

use Illuminate\Database\Eloquent\Model;

class Event_picture extends Model
{
    protected $fillable = ["file_name", "path", "priority", "event_id"];
    protected $dates = ["created_at", "updated_at"];
}
