<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/2/2017
 * Time: 9:09 PM
 */

namespace App\Http\Models\Comon;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = ["user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];
}