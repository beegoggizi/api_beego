<?php

namespace App\Http\Models\Comon;

use Illuminate\Database\Eloquent\Model;

class Gout extends Model
{
    protected $fillable = ['name', 'description'];
    protected $dates = ["created_at", "updated_at"];
}
