<?php

namespace App\Http\Models\Comon;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ["comment", "note", "user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];


}
