<?php

namespace App\Http\Models\Comon;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = ["user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];

    public function users()
    {
        return $this->belongsTo('App\Http\Models\Users\user', 'user_id', 'id');
    }

    public function espaces()
    {
        return $this->belongsTo('App\Http\Models\Espaces\Espace', 'espace_id', 'id');
    }
}
