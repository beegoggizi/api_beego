<?php

namespace App\Http\Models\Logs;

use Illuminate\Database\Eloquent\Model;

class Espace_trace extends Model
{
    protected $fillable = ["action", "description", "concerned_table", "affected_id", "user_id", "espace_id"];
    protected $dates = ["created_at", "updated_at"];
}
