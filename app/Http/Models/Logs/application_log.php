<?php

namespace App\Http\Models\Logs;

use Illuminate\Database\Eloquent\Model;

class application_log extends Model
{
    protected $fillable = ["bug_title", "description"];
    protected $dates = ["created_at", "updated_at"];
}
