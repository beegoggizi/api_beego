<?php

namespace App\Http\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class api_notifications extends Model
{
    protected $fillable = ['token', 'phone_id', 'app_id', 'user_id'];
    protected $dates = ['created_at', 'updated_at'];
}
