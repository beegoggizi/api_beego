<?php

namespace App\Http\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ["user_id", "event_id", "offer_id", "espace_id", "description", "title", "type", "seen"];
    protected $dates = ["created_at", "updated_at"];
}
