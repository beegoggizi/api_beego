<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/13/2017
 * Time: 9:26 PM
 */

namespace App\Http\Repositories\Log;


interface ITraceUser
{
    public function addTrace($user_id, $action, $description, $table_name, $table_id);
}