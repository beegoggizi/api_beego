<?php

namespace App\Http\Repositories\Log;


use App\Http\Models\Users\Trace_user;

class TraceUser implements ITraceUser
{

    public function addTrace($user_id, $action, $description, $table_name, $table_id)
    {
        $new = new Trace_user();
            $new->user_id = $user_id;
            $new->type = $action;
            $new->action_description = $description;
            $new->table_name = $table_name;
            $new->table_id = $table_id;
        $new->save();

        return $new;
    }

}