<?php

namespace App\Http\Repositories\Espaces;


use App\Http\Models\Comon\Favorite;
use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\EspacePosts;
use Illuminate\Http\Request;

class PostRepository implements IPostRepository
{
    public function addPost(Request $request)
    {
        $new = new EspacePosts();
            $new->espace_id = $request->espace_id;
            if(isset($request->title)) $new->title     = $request->title;
            $new->post = $request->post;
        $new->save();

        return $new;
    }

    public function updatePost(Request $request)
    {
        $new = EspacePosts::find($request->id);
            if(isset($request->title)) $new->title     = $request->title;
            if(isset($request->post))  $new->post = $request->post;
        $new->save();

        return $new;
    }

    public function deletePost($id)
    {
        EspacePosts::find($id)->delete();
    }

    public function getEspacePosts($espace_id)
    {
        return EspacePosts::where('espace_id', '=', $espace_id)->get();
    }

    public function postsByUser($user_id)
    {
        $follows  = Follow::where('user_id', '=', $user_id)->pluck('espace_id')->toArray();
        $favorits = Favorite::where('user_id', '=', $user_id)->pluck('espace_id')->toArray();
        $total = array_merge($follows, $favorits);
        $otherPosts = "";
        if(count($total) <= 10)
            $otherPosts = EspacePosts::orderBy('created_at', 'DESC')->take(20);
            $Userposts = EspacePosts::whereIn('espace_id', $total)->get();
        return ["status" => "success", "code" => "200", "userPosts" => $Userposts, "otherPosts" => $otherPosts];
    }
}