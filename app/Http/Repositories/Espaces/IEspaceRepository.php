<?php

namespace App\Http\Repositories\Espaces;


interface IEspaceRepository
{
    public function getEspaces($dist, $lng, $lat);
    public function getAllEspaces();
    public function getEspaceById($espace);
    public function update($espace);
    public function forceEspaceState($espace_id, $color);
    public function unforceEspaceState($espace_id);
    public function searchByName($name);
    public function getEspacePicture($id);
    public function getSuggestions();
    public function getSuggestionByEspaceId($espace_id);
    public function countSuggestions($id);
    public function getSuggestionEspaces($id);
    public function getOffersByEspaceId($espace_id);
    public function getEspaceNews($espace_id);
    public function getFollowersCount($espace_id);
    public function changeTime($horaires, $espace_id);
    public function getHoraires($espace_id);
}