<?php


namespace App\Http\Repositories\Espaces;


use App\Http\Models\Espaces\Espace_offer;
use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Pictures\Offer_picture;
use App\Http\Models\Users\User;
use App\Http\services;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class OfferRepository implements IOfferRepository
{
        public function get($espace_id)
        {
            return Espace_offer::where('espace_id', '=', $espace_id)->get();
        }

        public function add($offer)
        {
            $new = new Espace_offer();
                $new->espace_id = $offer->espace_id;
                $new->name = $offer->name;
                $new->description = $offer->description;
                if(isset($offer->start_date))  $new->start_date = $offer->start_date;
                if(isset($offer->end_date))  $new->end_date = $offer->end_date;
                if(isset($offer->start_at))  $new->start_at = $offer->start_at;
                if(isset($offer->end_at))  $new->end_at = $offer->end_at;
                if(isset($offer->minAge))   $new->minAge = $offer->minAge;
                if(isset($offer->maxAge))  $new->maxAge = $offer->maxAge;
                if(isset($offer->man))  $new->man = $offer->man;
                if(isset($offer->woman)) $new->woman = $offer->woman;
                if(isset($offer->image))
                {
                    if(strpos($offer->image, 'base64') !== false ){
                            $path     = url('pic/offers');
                            $fileName = "offer-".$new->id."-".time().".png";
                            $fullUrl  = $path."/".$fileName;
                            $new->image = $fullUrl;
                            Image::make($offer->image)->save('pic/offers/'.$fileName);
                            
                         }
                }
                
                $new->save();

            if(isset($offer->pictures))
            {
                $pictures = \GuzzleHttp\json_decode($offer->pictures);
                $i = 0;
                foreach ($pictures as $image) {
                     if(strpos($image, 'base64') !== false )
                     {
                        $path     = url('pic/offers');
                        $fileName = "offer-".$new->id."-".time().$i.".png";
                        $fullUrl  = $path."/".$fileName;

                        $picture = new Offer_picture();
                        $picture->path           = $fullUrl;
                        $picture->file_name      = $fileName;
                        $picture->priority       = 0;
                        $picture->offer_id       = $new->id;
                        $picture->created_at = Carbon::now();
                        Image::make($image)->save('pic/offers/'.$fileName);
                        $picture->save();
                        $i++;
                     }
                 }
            }

            $concerned_espace = Espace::find($offer->espace_id);
            $concerned_user = User::find($offer->user_id);

        $trace = new services();
        $action = "New offer";
        $description = $concerned_user->name." ".$concerned_user->last_name." has added a new offer in '".$concerned_espace->name."', the offer information are: ".$new;
        $trace->addEspaceTrace($offer->espace_id, $concerned_espace->user_id, $action, $description, "offers", $new->id);

        $concerned_users = Follow::where('espace_id', '=', $offer->espace_id)->pluck('user_id')->toArray();
        $devices = User::whereIn('id', $concerned_users)->pluck('device_id')->toArray();
        $trace->pushNotification("A new offer in ".$concerned_espace->name." start at ".$new->start_date." end at".$new->end_date, $devices);
        return $new;
}

    public function update($offer)
    {
        $new =  Espace_offer::find($offer->id);
                if(isset($offer->name)) $new->name = $offer->name;
                if(isset($offer->description))$new->description = $offer->description;
                if(isset($offer->start_date))  $new->start_date = $offer->start_date;
                if(isset($offer->end_date))  $new->end_date = $offer->end_date;
                if(isset($offer->start_at))  $new->start_at = $offer->start_at;
                if(isset($offer->end_at))  $new->end_at = $offer->end_at;
                if(isset($offer->minAge))   $new->minAge = $offer->minAge;
                if(isset($offer->maxAge))  $new->maxAge = $offer->maxAge;
                if(isset($offer->man))  $new->man = $offer->man;
                if(isset($offer->woman)) $new->woman = $offer->woman;
        $new->save();
        return $new;
    }

    public function delete($id)
    {
        Espace_offer::find($id)->delete();
        return "The offer has been deleted";
    }
}