<?php


namespace App\Http\Repositories\Espaces;


interface ISpecialityRepository
{

    public function add($speciality);
    public function update($speciality);
    public function delete($id);
    public function getEspaceSpecialities($espace_id);
    public function getUserSpecialities($user_id);
}