<?php

namespace App\Http\Repositories\Espaces;


use App\Http\Models\Comon\Favorite;
use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\Espace_speciality;
use App\Http\Models\Espaces\SpecialityPicture;

class SpecialityRepository implements ISpecialityRepository
{
    public function add($speciality)
    {
        $spec = new Espace_speciality();
            $spec->name = $speciality->name;
            $spec->espace_id = $speciality->espace_id;
            $spec->description = $speciality->description;
         $spec->save();
        if(isset($speciality->picture))
        {   $image = $speciality->picture;
                if(strpos($image, 'base64') !== false )
                {
                    $path     = url('pic/specialities');
                    $fileName = "speciality-".$spec->id."-".time().".png";
                    $fullUrl  = $path."/".$fileName;

                    $picture = new SpecialityPicture();
                        $picture->path           = $fullUrl;
                        $picture->file_name      = $fileName;
                        $picture->speciality_id       = $spec->id;
                        Image::make($image)->save('pic/specialities/'.$fileName);
                    $picture->save();
                }
        }

        return $spec;
    }

    public function update($speciality)
    {
        $spec =  Espace_speciality::find($speciality->id);
            $spec->name = $speciality->name;
            $spec->description = $speciality->description;
        $spec->save();
        if(isset($speciality->pictures))
        {   $image = $speciality->pictures;
            if(strpos($image, 'base64') !== false )
            {
                $path     = url('pic/specialities');
                $fileName = "speciality-".$spec->id."-".time().".png";
                $fullUrl  = $path."/".$fileName;

                $picture =  SpecialityPicture::where('speciality_id', '=', $spec->id);
                \File::delete($picture->path);
                    $picture->path           = $fullUrl;
                    $picture->file_name      = $fileName;
                    Image::make($image)->save('pic/specialities/'.$fileName);
                $picture->save();
            }
        }
        return $spec;
    }

    public function delete($id)
    {
         Espace_speciality::find($id)->delete();
        return "Speciality has been deleted";
    }

    public function getEspaceSpecialities($espace_id)
    {
        $results = Espace_speciality::where('espace_id', '=', $espace_id)->select("id","name", "description", "espace_id")->get();

        foreach ($results as $result)
            $result->picture = $this->getSpecialityPicture($result->id);

        return $results;
    }

    public function getUserSpecialities($user_id)
    {
        $follow_ids = Follow::where('user_id', '=', $user_id)->pluck('espace_id')->toArray();
        $favorite_ids = Favorite::where('user_id', '=', $user_id)->pluck('espace_id')->toArray();
        $result = array_unique(array_merge($follow_ids,$favorite_ids));
        return Espace_speciality::whereIn('espace_id', $result)->get();

    }

      public function getSpecialityPicture($id)
    {
       if($data = SpecialityPicture::where('speciality_id', '=', $id)->first())
            return $data->path;
       else return null;
    }
}