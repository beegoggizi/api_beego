<?php


namespace App\Http\Repositories\Espaces;


use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Espaces\Espace_event;
use App\Http\Models\Pictures\Event_picture;
use App\Http\Models\Users\User;
use App\Http\Models\Users\UserEvent;
use App\Http\services;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class EventRepository implements IEventRepository
{
    public function addEvent($request)
    {
        $new = new Espace_event();
            $new->espace_id = $request->espace_id;
            $new->user_id   = $request->user_id;
            $new->title    = $request->title;
            $new->description = $request->description;
            $new->visible = 1;
            $new->event_date = $request->event_date;
            $new->start_at  = $request->start_at;
            $new->end_at   = $request->end_at;
            if(isset($request->end_date))       $new->end_date   = $request->end_date;
            if(isset($request->lieu))           $new->lieu   = $request->lieu;
            if(isset($request->inviter))        $new->inviter  = $request->inviter;
            if(isset($request->tags)) $new->tags = $request->tags;
            if(isset($request->image))
            {
                if(strpos($request->image, 'base64') !== false ){
                        $path     = url('pic/events');
                        $fileName = "event-".$new->id."-".time().".png";
                        $fullUrl  = $path."/".$fileName;
                        $new->image = $fullUrl;
                        Image::make($request->image)->save('pic/events/'.$fileName);
                        
                     }

            }

            $new->save();

        if(isset($request->pictures))
         {
                $pictures = \GuzzleHttp\json_decode($request->pictures);
                $i = 0;
                foreach ($pictures as $image) {
                	 if(strpos($image, 'base64') !== false )
           			 {
	                    $path     = url('pic/events');
	                    $fileName = "event-".$new->id."-".time().$i.".png";
	                    $fullUrl  = $path."/".$fileName;

                   	    $picture = new Event_picture();
                        $picture->path           = $fullUrl;
                        $picture->file_name      = $fileName;
                        $picture->priority       = 0;
                        $picture->event_id       = $new->id;
                        $picture->created_at = Carbon::now();
                        Image::make($image)->save('pic/events/'.$fileName);
                    	$picture->save();
                    	$i++;
               		 }
           		}
         }

        $concerned_espace = Espace::find($request->espace_id);
        $concerned_user = User::find($request->user_id);

        $trace = new services();
        $action = "New Event";
        $description = $concerned_user->name." ".$concerned_user->last_name." has added a new event in '".$concerned_espace->name."', the event information are: ".$new;
        $trace->addEspaceTrace($request->espace_id, $concerned_espace->user_id, $action, $description, "events", $new->id);

        if($request->inviter == 1)
        {
            $concerned_users = Follow::where('espace_id', '=', $request->espace_id)->pluck('user_id')->toArray();
            $devices = User::whereIn('id', $concerned_users)->pluck('device_id')->toArray();
             $trace->pushNotification("A new event in ".$concerned_espace->name." at ".$new->event_date, $devices);
        }

        return $new;
    }

    public function updateEvent($request)
    {
        $new = Espace_event::find($request->id);
                if(isset($request->title))      $new->title    = $request->title;
                if(isset($request->description))$new->description = $request->description;
                if(isset($request->event_date)) $new->event_date = $request->event_date;
                if(isset($request->start_at))   $new->start_at  = $request->start_at;
                if(isset($request->end_at))     $new->end_at   = $request->end_at;
                if(isset($request->tags))       $new->tags = $request->tags;
                if(isset($request->visible))    $new->visible = $request->visible;
                if(isset($request->end_date))       $new->end_date   = $request->end_date;
                if(isset($request->lieu))           $new->lieu   = $request->lieu;
                if(isset($request->inviter))        $new->inviter  = $request->inviter;
                $new->updated_at = Carbon::now();
        $new->save();

        $concerned_espace = Espace::find($new->espace_id);
        $concerned_user = User::find($request->user_id);

        $trace = new services();
        $action = "Event Updated";
        $description = $concerned_user->name." ".$concerned_user->last_name." has updated an event in '".$concerned_espace->name."', the new information are: ".$new;
        $trace->addEspaceTrace($new->espace_id, $concerned_espace->user_id, $action, $description, "events", $new->id);

        return $new;
    }

    public function delete($id)
    {
       $event = Espace_event::find($id);

        $concerned_espace = Espace::find($event->espace_id);
        $concerned_user = User::find($event->user_id);

        $trace = new services();
        $action = "Event Deleted";
        $description = $concerned_user->name." ".$concerned_user->last_name." has deleted an event in '".$concerned_espace->name."', the deleted information are: ".$event;
        $trace->addEspaceTrace($event->espace_id, $concerned_espace->user_id, $action, $description, "events", $event->id);

        $event->delete();
    }



    public function getEspaceEvent($espace_id)
    {
        return Espace_event::where('espace_id', '=', $espace_id)->get();

    }

    public function getAll()
    {
        $events = Espace_event::where('event_date', '>=', date("Y-m-d"))->orderBy('created_at', 'DESC')->get();

        foreach ($events as $event)
                $event->interrested_users = $this->getInterrested($event->id);

        return $events;
    }

    public function getInterrested($event_id)
    {
        $interrested_id = UserEvent::where('event_id', '=', $event_id)->pluck('user_id')->toArray();
        $interrested_users = User::whereIn('id', $interrested_id)->get();

        return $interrested_users;
    }

    public function userInterrested($user_id, $event_id)
    {
        $new = new UserEvent();
            $new->user_id = $user_id;
            $new->event_id = $event_id;
        $new->save();
        return $new;
    }
}