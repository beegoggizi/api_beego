<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/11/2017
 * Time: 8:29 PM
 */

namespace App\Http\Repositories\Espaces;


use Illuminate\Http\Request;

interface IPostRepository
{
    public function deletePost($id);
    public function updatePost(Request $request);
    public function addPost(Request $request);
    public function getEspacePosts($espace_id);

    public function postsByUser($user_id);

}