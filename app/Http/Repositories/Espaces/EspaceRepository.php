<?php

namespace App\Http\Repositories\Espaces;


use App\Http\Models\Comon\Follow;
use App\Http\Models\Comon\Suggestion;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Espaces\Espace_config;
use App\Http\Models\Espaces\Espace_horaires;
use App\Http\Models\Espaces\Espace_offer;
use App\Http\Models\Espaces\EspaceSuggestion;
use App\Http\Models\Espaces\News;
use App\Http\Models\Pictures\Espace_picture;
use App\Http\Models\Users\User;
use App\Http\services;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class EspaceRepository implements IEspaceRepository
{

    public function getEspaces($dist, $lng, $lat)
    {
    $test = DB::select('select id, (
                              3959 * acos (
                              cos ( radians('.$lat.') )
                              * cos( radians( latitude ) )
                              * cos( radians( longitude ) - radians('.$lng.') )
                              + sin ( radians('.$lat.') )
                              * sin( radians( latitude ) )
                            )
                        ) AS distance
                        FROM espaces
                        HAVING distance < '.$dist.'
                        ORDER BY distance
                        LIMIT 0 , 20;');

        $results = Espace::whereIn('id', collect($test)->pluck('id')->toArray())
                            ->get();
        foreach ($results as $result)
            $result->profile_picture = $this->getEspacePicture($result->id);

        return $results;
    }

    public function getAllEspaces()
    {
        $results = Espace::with( 'pictures',  'horaires', 'specialities', 'config')->get();
        foreach ($results as $result)
            $result->profile_picture = $this->getEspacePicture($result->id);

        return $results;
    }

    public function getEspaceById($espace)
    {
        $result = Espace::with('offers', 'pictures', 'offers',  'horaires', 'specialities', 'events', 'config')->find($espace);
        $result->profile_picture = $this->getEspacePicture($result->id);
        return $result;
    }

 public function update($espace)
    {
        $update = Espace::find($espace->id);
            if(isset($espace->name))        $update->name = $espace->name;
            if(isset($espace->address))     $update->address = $espace->address;
            if(isset($espace->gouvernorat)) $update->gouvernorat = $espace->gouvernorat;
            if(isset($espace->description)) $update->description = $espace->description;

            //set image as cover picture
        if(isset($espace->cover))
        {
            if (strpos($espace->cover, 'base64') !== false )
            {
                $path     = url('pic/espaces');
                $fileName = "espace-".$espace->id."-".time().".png";
                $fullUrl  = $path."/".$fileName;
                $picture = new Espace_picture();
                        $picture->path           = $fullUrl;
                        $picture->file_name      = $fileName;
                        $picture->priority       = 0;
                        $picture->espace_id      = $espace->id;
                        $picture->created_at = Carbon::now();
                        Image::make($espace->cover)->save('pic/espaces/'.$fileName);
                        $picture->save();
                        $update->cover = $fullUrl;
                
            }
            else if(is_numeric ( $espace->cover ))
            {
                $picture = Espace_picture::find($espace->cover);
                $update->cover = $picture->path;
            }
        }

            //set image as profile picture
                if(isset($espace->profile_pic))
                 {
                    if (strpos($espace->profile_pic, 'base64') !== false )
                    {
                        $path     = url('pic/espaces');
                        $fileName = "espace-".$espace->id."-".time().".png";
                        $fullUrl  = $path."/".$fileName;

                        $picture = Espace_picture::where('espace_id', '=', $espace->id)
                                                ->where('priority', '=', 1)
                                                ->first();
                        if($picture)
                            {
                                $picture->priority = 0;
                                $picture->save();
                            }

                            $picture = new Espace_picture();

                                $picture->path           = $fullUrl;
                                $picture->file_name      = $fileName;
                                $picture->priority       = 1;
                                $picture->espace_id      = $espace->id;
                                $picture->created_at = Carbon::now();
                                Image::make($espace->profile_pic)->save('pic/espaces/'.$fileName);
                        $picture->save();
                    }
                    else if(is_numeric ( $espace->profile_pic ))
                    {
                        $picture = Espace_picture::where('espace_id', '=', $espace->id)
                                                ->where('priority', '=', 1)
                                                ->first();
                        if($picture)
                            {
                                $picture->priority = 0;
                                $picture->save();
                            }
                        $picture = Espace_picture::find($espace->profile_pic);
                        $picture->priority = 1;
                        $picture->save();
                    }
            $update->picture = $picture->id;
            }

            //add photo
              if(isset($espace->picture))
               { 
                $image = $espace->picture;
                     if(strpos($image, 'base64') !== false )
                     {
                        $path     = url('pic/espaces');
                        $fileName = "espace-".$espace->id."-".time().".png";
                        $fullUrl  = $path."/".$fileName;

                        $picture = new Espace_picture();
                        $picture->path           = $fullUrl;
                        $picture->file_name      = $fileName;
                        $picture->priority       = 0;
                        $picture->espace_id      = $espace->id;
                        $picture->created_at = Carbon::now();
                        Image::make($image)->save('pic/espaces/'.$fileName);
                        $picture->save();
                     }
            }

        $concerned_espace = Espace::find($espace->id);
        $concerned_user = User::find($concerned_espace->user_id);

        $trace = new services();
        $action = "Espace updated";
        $description = $concerned_user->name." ".$concerned_user->last_name." has updated ".$concerned_espace->name.", the updated information are: ".$espace;
        $trace->addEspaceTrace($espace->id, $concerned_espace->user_id, $action, $description, "espaces", $concerned_espace->id);
        $trace->EspaceHistory($concerned_espace->id, 'Vous avez mis à jour les informations de votre espace');


        if($update->save()) {
            if(isset($espace->profile_pic))
            $update->profile_pic = $picture->path;
            else if(isset($espace->picture)){
                $update->picture_id = $picture->id;
                $update->path = $picture->path;
            }
            return $update;
        }

        else return "update fail";
    }

    public function forceEspaceState($espace_id, $color)
    {
        $config = Espace_config::Where('espace_id', '=', $espace_id)->first();
            $config->isForced = 1;
            $config->forcedColor = $color;
         $config->save();

        $concerned_espace = Espace::find($espace_id);
        $concerned_user = User::find($concerned_espace->user_id);

        $trace = new services();
        $action = "Espace forced";
        $description = $concerned_user->name." ".$concerned_user->last_name." has forced ".$concerned_espace->name." with the color ".$color;
        $trace->addEspaceTrace($espace_id, $concerned_espace->user_id, $action, $description, "espaces", $concerned_espace->id);
        $trace->EspaceHistory($concerned_espace->id, 'Vous avez forcer de l\'état de votre espace');

        return $config;
    }

    public function unforceEspaceState($espace_id)
    {
        $config = Espace_config::Where('espace_id', '=', $espace_id)->first();
            $config->isForced = 0;
            $config->forcedColor = null;
        $config->save();

        $concerned_espace = Espace::find($espace_id);
        $concerned_user = User::find($concerned_espace->user_id);

        $trace = new services();
        $action = "Espace unforced";
        $description = $concerned_user->name." ".$concerned_user->last_name." has unforced ".$concerned_espace->name;
            $trace->addEspaceTrace($espace_id, $concerned_espace->user_id, $action, $description, "espaces", $concerned_espace->id);
            $trace->EspaceHistory($concerned_espace->id, 'Vous avez annuler le forçage de l\'état de votre espace');


        return $config;
    }

    public function searchByName($name)
    {
        $results = Espace::where('name', 'like', '%'.$name.'%')->get();
        foreach ($results as $result)
        {
            $result->profile_picture = $this->getEspacePicture($result->id);
        }
        return $results;
    }

    public function getEspacePicture($id)
    {
       if($data = Espace_picture::where('espace_id', '=', $id)->where('priority', '=', 1)->first())
            return $data->path;
       else return null;
    }

    public function getSuggestions()
    {
        $suggestions = Suggestion::all();
        foreach ($suggestions as $suggestion)
            $suggestion->numberOfEspaces = $this->countSuggestions($suggestion->id);

        return $suggestions;
    }

    public function getSuggestionByEspaceId($espace_id)
    {
        $suggestions = EspaceSuggestion::where('espace_id', '=', $espace_id)->pluck('suggestion_id')->toArray();
        return Suggestion::whereIn('id', $suggestions)->get();
    }

    public function countSuggestions($id)
    {
        return EspaceSuggestion::where('suggestion_id', '=', $id)->count();
    }

    public function getSuggestionEspaces($id)
    {
        $ids = EspaceSuggestion::where('suggestion_id', '=', $id)->pluck('espace_id')->toArray();
        return Espace::with('horaires')->whereIn('id', $ids)->paginate(5);
    }

    public function getOffersByEspaceId($espace_id)
    {
        return Espace_offer::with('pictures')->where('espace_id', '=', $espace_id)->orderBy('created_at', 'DESC')->get();
    }

    public function getEspaceNews($espace_id)
    {
        return News::where('espace_id', '=', $espace_id)->orderBy('created_at', 'DESC')->get();
    }

    public function getFollowersCount($espace_id)
    {
        return Follow::where('espace_id', '=', $espace_id)->count();
    }

    public function changeTime($horaires, $espace_id)
    {
        // Espace_horaires::where('espace_id', '=', $espace_id)->delete();
        $i=0;
        foreach ($horaires as $horaire) {

            $data = Espace_horaires::where('day', '=', $horaire['day'])->where('espace_id', '=', $espace_id)->first();
            if(!$data)
                $data = new Espace_horaires();

                $data->espace_id = $espace_id;
                $data->day = $horaire['day'];
                $data->open_at = $horaire['start_at'];
                $data->close_at = $horaire['end_at'];
            $data->save();
            $i++;
        }
        return $i.' days added';
    }

    public function getHoraires($espace_id)
    {
        return Espace_horaires::where('espace_id', '=', $espace_id)->get();
    }

}