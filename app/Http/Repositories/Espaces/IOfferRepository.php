<?php

namespace App\Http\Repositories\Espaces;


interface IOfferRepository
{
    public function get($espace_id);
    public function add($offer);
    public function update($offer);
    public function delete($id);
}