<?php


namespace App\Http\Repositories\Espaces;


interface IEventRepository
{
    public function addEvent($request);
    public function updateEvent($request);
    public function delete($id);
    public function getEspaceEvent($espace_id);
    public function getAll();
    public function getInterrested($event_id);
    public function userInterrested($user_id, $event_id);
}