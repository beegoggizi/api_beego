<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 11/25/2017
 * Time: 11:57 AM
 */

namespace App\Http\Repositories\Notifications;


interface IEloquentNotificationRepository
{
    public function neweventNotification($user, $espace, $event);
    public function newofferNotification($user, $espace, $offer);
    public function newfollowNotification($user, $espace);
    public function newProgramNotification($program);


    public function getEspaceNotifications($espace);
    public function getUserNotifications($user);
    public function userNotifsTypes();
    public function espaceNotifsTypes();
}