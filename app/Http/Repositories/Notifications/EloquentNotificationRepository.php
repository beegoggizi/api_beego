<?php

namespace App\Http\Repositories\Notifications;


use App\Http\Models\Espaces\Espace;
use App\Http\Models\Espaces\Espace_event;
use App\Http\Models\Espaces\Espace_offer;
use App\Http\Models\Notifications\Notification;
use App\Http\Models\Users\ProgramInvit;
use App\Http\Models\Users\Programme;
use App\Http\Models\Users\User;
use App\Http\Repositories\Checks\ICheckRepository;

class EloquentNotificationRepository implements IEloquentNotificationRepository
{   protected $checks;

    public function __construct(ICheckRepository $check)
    {
        $this->checks = $check;
    }

    public function newfollowNotification($user, $espace)
    {
      $follower = User::find($user);
          $notification=  Notification::create([
               'user_id' => $user,
               'espace_id' => $espace,
               'type' => 'follow',
               'title' => 'New follower',
               'description' => $follower->name.' '.$follower->last_name.' follow you, he will receive all your posts (events, offers...)',
           ]);
           return ['status' => 'success', 'code' => '200', 'msg' => 'notification created', 'notification' => $notification];
    }

    public function neweventNotification( $user, $espace, $event)
    {
        $concernedEspace = Espace::find($espace);
        $concernedEvent = Espace_event::find($event);
        $notification=  Notification::create([
            'user_id' => $user,
            'espace_id' => $espace,
            'event_id' => $event,
            'type' => 'event',
            'title' => 'New event',
            'description' => 'A new event in '.$concernedEspace->name.' at '.$concernedEvent->event_date,
        ]);
        return ['status' => 'success', 'code' => '200', 'msg' => 'notification created', 'notification' => $notification];
    }

    public function newofferNotification($user, $espace, $offer)
    {
        $concernedEspace = Espace::find($espace);
        $concernedOffer = Espace_offer::find($offer);
        $notification=  Notification::create([
            'user_id' => $user,
            'espace_id' => $espace,
            'offer_id' => $offer,
            'type' => 'offer',
            'title' => 'New offer',
            'decription' => 'A new offer in '.$concernedEspace->name.' : '.$concernedOffer->name,
        ]);
        return ['status' => 'success', 'code' => '200', 'msg' => 'notification created', 'notification' => $notification];

    }

    public function newProgramNotification($program)
    {
        $invited_ids = ProgramInvit::where('program_id', '=', $program->id)->pluck('invited')->toArray();
        $concernedUser = User::find($program->user_id);
        $concernedEspace = Espace::find($program->espace_id);

            foreach ($invited_ids as $invited_id)
            {
                $notification= new Notification();
                    $notification->user_id = $invited_id;
                    $notification->espace_id = $program->espace_id;
                    $notification->type = 'program';
                    $notification->title = 'New program';
                    $notification->description = 'You are invited to a new program in '.$concernedEspace->name.' : organized by '.$concernedUser->name.' '.$concernedUser->last_name;
                $notification->save();
            }
        return ['status' => 'success', 'code' => '200', 'msg' => 'notification created'];
    }

    public function getEspaceNotifications($espace)
    {
        $espaceExist = $this->checks->espaceExist($espace);
        if($espaceExist)
        {   $notifTypes = $this->espaceNotifsTypes();
            $unseen = Notification::where('espace_id', '=', $espace)
                                         ->where('seen', '=', 0)
                                         ->whereIn('type', $notifTypes)
                                         ->select('id', 'user_id', 'seen', 'title', 'description', 'espace_id','created_at')
                                         ->orderBy('created_at', 'DESC')
                                         ->get();
            $seen  = Notification::where('espace_id', '=', $espace)
                                        ->where('seen', '=', 1)
                                        ->whereIn('type', $notifTypes)
                                        ->select('id', 'user_id', 'seen', 'title', 'description', 'espace_id','created_at')
                                        ->orderBy('created_at', 'DESC')
                                        ->get()
                                        ->take(10);

            foreach ($unseen as $uns)
            {
                $user = User::find($uns->user_id);
                    $uns->seen=1;
                    $uns->save();
                $uns->user_name = $user->name;
                $uns->user_lastName = $user->last_name;
                $uns->user_pic = $user->picture_path;
                 unset($uns->user_id); unset($uns->seen);
            }
            foreach ($seen as $uns)
            {
                $user = User::find($uns->user_id);
                $uns->user_name = $user->name;
                $uns->user_lastName = $user->last_name;
                $uns->user_pic = $user->picture_path;
                unset($uns->user_id); unset($uns->seen);
            }

            return ['status' => 'success', 'code' => '200', 'unseenNotifs' => $unseen, 'seenNotifs' => $seen];

        }
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No espaces with the indicated espace id'];
    }

    public function getUserNotifications($user)
    {
        $userExist = $this->checks->userExist($user);
        if($userExist)
        {
            $notifTypes = $this->userNotifsTypes();
            $unseen = Notification::where('user_id', '=', $user)
                                        ->where('seen', '=', 0)
                                        ->whereIn('type', $notifTypes)
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

            $seen  = Notification::where('user_id', '=', $user)
                                        ->where('seen', '=', 1)
                                        ->whereIn('type', $notifTypes)
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

            return ['status' => 'success', 'code' => '200', 'unseenNotifs' => $unseen, 'seenNotifs' => $seen];
        }
        else return ['status' => 'error', 'code' => '404', 'msg' => 'No users with the indicated user id'];
    }

    public function userNotifsTypes()
    {
        return ['event', 'offer', 'special_client', 'all', 'program'];
    }

    public function espaceNotifsTypes()
    {
       return ['follow', 'feedback', 'special_espace', 'all'];
    }
}