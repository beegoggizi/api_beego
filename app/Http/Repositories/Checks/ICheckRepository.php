<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 11/25/2017
 * Time: 12:18 PM
 */

namespace App\Http\Repositories\Checks;


interface ICheckRepository
{
    public function isUserAlreadyFollow($user, $espace);
    public function userExist($user);
    public function espaceExist($espace);
    public function isUserAlreadyFeedbacked($user, $espace);
    public function eventExist($event);
    public function postExist($id);
    public function goutExist($id);
    public function espaceGoutExist($espace_id, $gout_id);
    public function userGoutExist($user_id, $gout_id);
    public function userAlreadyInterrestedEvent($user_id, $event_id);
    public function isUserAlreadyFollowUser($follower_id, $followed_id);
}