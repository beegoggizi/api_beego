<?php

namespace App\Http\Repositories\Checks;


use App\Http\Models\Comon\Feedback;
use App\Http\Models\Comon\Follow;
use App\Http\Models\Comon\Gout;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Espaces\Espace_event;
use App\Http\Models\Espaces\espace_gout;
use App\Http\Models\Espaces\EspacePosts;
use App\Http\Models\Users\User;
use App\Http\Models\Users\user_gout;
use App\Http\Models\Users\UserEvent;
use App\Http\Models\Users\UserFollow;

class CheckRepository implements ICheckRepository
{

    public function isUserAlreadyFollow($user, $espace)
    {
        if($exist = Follow::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first())
            return true;
        else return false;
    }

    public function isUserAlreadyFollowUser($follower_id, $followed_id)
    {
        if($exist = UserFollow::where('follower_id', '=', $follower_id)->where('user_id', '=', $followed_id)->first())
            return true;
        else return false;
    }

    public function isUserAlreadyFeedbacked($user, $espace)
    {
        if($exist = Feedback::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first())
            return true;
        else return false;
    }

    public function userExist($user)
    {
        if($exist = User::where('id', '=', $user)->first())
            return true;
        else return false;
    }

    public function espaceExist($espace)
    {
        if($exist = Espace::where('id', '=', $espace)->first())
            return true;
        else return false;
    }

    public function eventExist($event)
    {
        if($exist = Espace_event::where('id', '=', $event)->first())
            return true;
        else return false;
    }

    public function postExist($id)
    {
        if($exist = EspacePosts::where('id', '=', $id)->first())
            return true;
        else return false;
    }

    public function goutExist($id)
    {
        if($exist = Gout::where('id', '=', $id)->first())
            return true;
        else return false;
    }

    public function espaceGoutExist($espace_id, $gout_id)
    {
        if($exist = espace_gout::where('espace_id', '=', $espace_id)->where('gout_id', '=', $gout_id)->first())
            return true;
        else return false;
    }

    public function userGoutExist($user_id, $gout_id)
    {
        if($exist = user_gout::where('user_id', '=', $user_id)->where('gout_id', '=', $gout_id)->first())
            return true;
        else return false;
    }

    public function userAlreadyInterrestedEvent($user_id, $event_id)
    {
        if($exist = UserEvent::where('user_id', '=', $user_id)->where('event_id', '=', $event_id)->first())
            return true;
        else return false;
    }
}