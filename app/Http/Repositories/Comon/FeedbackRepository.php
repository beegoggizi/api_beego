<?php
namespace App\Http\Repositories\Comon;

use App\Http\Models\Comon\Feedback;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Users\User;

class FeedbackRepository implements IFeedbackRepository
{

    public function addFeedback($user, $espace, $comment, $note, $check)
    {
        if(!$check)
        {
            Feedback::create([
                'user_id' => $user,
                'espace_id' => $espace,
                'comment' => $comment,
                'note'    => $note,
            ]);
            return ['Status' => 'success', 'code' => '200',
                'user_id' => $user, 'espace_id' => $espace,
                'msg' => 'The feedback has been registred'
            ];
        }
        else return ['Status' => 'error', 'code' => '308',
            'user_id' => $user, 'espace_id' => $espace,
            'msg' => 'The user has already add a feedback for that espace'
        ];

    }

    public function deleteFeedback($user, $espace)
    {
        if($exist = Feedback::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first())
        {
            $exist->delete();
            return ['Status' => 'success', 'code' => '200',
                'user_id' => $user, 'espace_id' => $espace,
                'msg' => 'The feedback has been deleted'
            ];
        }
        else return ['Status' => 'Error', 'code' => '404',
            'user_id' => $user, 'espace_id' => $espace,
            'msg' => 'The indicated couple (user,espace) does not exist in the feedback table, Maybe you pressed 2 times on the button'
        ];
    }

    public function deleteFeedbackById($id)
    {
        if($exist = Feedback::find($id))
        {
            $exist->delete();
            return ['Status' => 'success', 'code' => '200',
                'msg' => 'The feedback has been deleted'
            ];
        }
        else return ['Status' => 'Error', 'code' => '404',
            'msg' => 'No feedback with the given Id'
        ];
    }

    public function espaceFeedbacks($espace)
    {
        $usersId = Feedback::where('espace_id', '=', $espace)->pluck('user_id')->toArray();
        $users = User::whereIn('id', $usersId)->get();

        foreach ($users as $user)
        {
                $user->feedback = Feedback::where('user_id', '=', $user->id)->where('espace_id', '=', $espace)->first();
        }

        return ['Status' => 'success', 'code' => '200',
            'espace_id' => $espace,
            'Feedbackers' => $users,
            'msg' => 'Here the list of the users whith their feedbacks the indicated espace id'
        ];
    }

    public function userFeedback($user)
    {
        $espacesId = Feedback::where('user_id', '=', $user)->pluck('espace_id')->toArray();
        $espaces = Espace::whereIn('id', $espacesId)->get();

        foreach ($espaces as $espace)
        {
            $espace->feedback = Feedback::where('user_id', '=', $user)->where('espace_id', '=', $espace->id)->first();
        }

        return ['Status' => 'success', 'code' => '200',
            'user_id' => $user,
            'feedbacks' => $espaces,
            'msg' => 'Here the list of the feedback for the given user id'
        ];
    }

    public function updateFeedback($user, $espace, $comment, $note)
    {
        $exist = Feedback::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first();
        if($exist)
        {
            $exist->comment = $comment;
            $exist->note = $note;
            $exist->save();

            return ['Status' => 'success', 'code' => '200',
                'feedback' => $exist,
                'msg' => 'Feedback updated'
            ];
        }
        return ['Status' => 'error', 'code' => '404',
            'user_id' => $user,
            'espace_id' => $espace,
            'msg' => 'no feedback with the given couple (user_id, espace_id) !'
        ];
    }

    public function updateFeedbackById($id, $comment, $note)
    {
        $exist = Feedback::find($id);
        if($exist)
        {
            $exist->comment = $comment;
            $exist->note = $note;
            $exist->save();

            return ['Status' => 'success', 'code' => '200',
                'feedback' => $exist,
                'msg' => 'Feedback updated'
            ];
        }
        return ['Status' => 'error', 'code' => '404',
            'id' => $id,
            'msg' => 'no feedback with the given Id!'
        ];
    }

    public function like($id, $like)
    {
        $feedback = Feedback::find($id);

        if($like == 1) $feedback->like = 1;
        else $feedback->like = 0;

        $feedback->save();
        return ['Status' => 'success', 'code' => '200',
            'feedback' => $feedback,
            'msg' => 'Done'
        ];
    }
}