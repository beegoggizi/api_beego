<?php

namespace App\Http\Repositories\Comon;


use App\Http\Models\Comon\Follow;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Users\User;
use App\Http\Models\Users\UserFollow;

class EloquentFollowRepository implements IEloquentFollowRepository
{

    public function follow($user, $espace, $check)
    {
        if(!$check)
        {
            Follow::create([
                'user_id' => $user,
                'espace_id' => $espace,
            ]);
            return ['Status' => 'success', 'code' => '200',
                'user_id' => $user, 'espace_id' => $espace,
                'msg' => 'The user now follow the indicated espace'
            ];
        }
        else return ['Status' => 'error', 'code' => '308',
                        'user_id' => $user, 'espace_id' => $espace,
                        'msg' => 'The user already follow the indicated espace'
                    ];

    }

    public function unfollow($user, $espace)
    {
        if($exist = Follow::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first())
        {
            $exist->delete();
            return ['Status' => 'success', 'code' => '200',
                'user_id' => $user, 'espace_id' => $espace,
                'msg' => 'The user don\'t follow the indicated espace anymore'
            ];
        }
        else return ['Status' => 'Error', 'code' => '404',
            'user_id' => $user, 'espace_id' => $espace,
            'msg' => 'The indicated couple (user,espace) does not exist in the follow table, did the user started following that espace before ?'
        ];
    }

    public function followers($espace)
    {
        $followersId = Follow::where('espace_id', '=', $espace)->pluck('user_id')->toArray();
        $users = User::whereIn('id', $followersId)->get();

        return ['Status' => 'success', 'code' => '200',
                'espace_id' => $espace,
                'followers' => $users,
                'msg' => 'Here the list of the users who follow the indicated espace id'
                ];
    }

    public function follows($user)
    {
        $followedId = Follow::where('user_id', '=', $user)->pluck('espace_id')->toArray();
        $espaces = Espace::whereIn('id', $followedId)->get();
        return ['Status' => 'success', 'code' => '200',
            'user_id' => $user,
            'follows' => $espaces,
            'msg' => 'Here the list of the espaces that the indicated user follow'
        ];
    }

    public function userFollow($follower_id, $user_id)
    {
        $new = new UserFollow();
            $new->follower_id = $follower_id;
            $new->user_id = $user_id;
        $new->save();
        return $new;
    }

    public function accept($id)
    {
        $data = UserFollow::find($id);
        $data->status = 1;
        $data->save();
        return 'Success ! Accepted';
    }

    public function wait($id)
    {
        $data = UserFollow::find($id);
        $data->status = 2;
        $data->save();
        return 'Success ! The request is in wait';
    }

    public function refuse($id)
    {
         UserFollow::find($id)->delete();
        return 'Success ! Request Refused';
    }

    public function myWaitingList($user_id)
    {
        return UserFollow::where('user_id', '=', $user_id)->where('status', '=', 2)->get();
    }

    public function myPendingRequests($user_id)
    {
        return UserFollow::where('follower_id', '=', $user_id)->where('status', '=', 2)->get();
    }

    public function userUnfollow($follower_id, $user_id)
    {
        UserFollow::where('follower_id', '=', $follower_id)->where('user_id', '=', $user_id)->delete();
        return 'The user stopped following the given user id';
    }

    public function getUserFollowers($user_id)
    {
        $followers_id = UserFollow::where('user_id', '=', $user_id)->where('status', '=', 1)->pluck('follower_id')->toArray();
        $followers = User::whereIn('id', $followers_id)->get();
        return $followers;
    }

    public function searchFollower($espace_id, $ch)
    {
        $ids = Follow::where('espace_id', '=', $espace_id)->pluck('user_id')->toArray();
        $users = User::whereIn('id', $ids)->where(function ($query) use ($ch) {
                                    $query->where('name', 'like', '%'.$ch.'%')->orWhere('last_name', 'like', '%'.$ch.'%');
                            })->paginate(5);
        return $users;
    }

}