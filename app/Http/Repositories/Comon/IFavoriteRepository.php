<?php


namespace App\Http\Repositories\Comon;


interface IFavoriteRepository
{
    public function getUserFavorite($user);
    public function getEspaceFavorite($espace);
    public function deleteFavorite($user, $espace);
    public function deleteFavoriteById($id);
    public function addFavorite($user, $espace);
}