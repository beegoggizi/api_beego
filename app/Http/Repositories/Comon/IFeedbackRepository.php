<?php


namespace App\Http\Repositories\Comon;


interface IFeedbackRepository
{
    public function addFeedback($user, $espace, $comment, $note, $check);
    public function deleteFeedback($user, $espace);
    public function deleteFeedbackById($id);
    public function espaceFeedbacks($espace);
    public function userFeedback($user);
    public function updateFeedback($user, $espace, $comment, $note);
    public function updateFeedbackById($id, $comment, $note);
    public function like($id, $like);
}