<?php


namespace App\Http\Repositories\Comon;


interface IEloquentFollowRepository
{
    public function follow($user, $espace, $check);
    public function unfollow($user, $espace);
    public function followers($espace);
    public function follows($user);
    public function userFollow($follower_id, $user_id);
    public function userUnfollow($follower_id, $user_id);
    public function getUserFollowers($user_id);
    public function searchFollower($espace_id, $query);
    public function accept($id);
    public function wait($id);
    public function myPendingRequests($user_id);
    public function myWaitingList($user_id);
    public function refuse($id);
}