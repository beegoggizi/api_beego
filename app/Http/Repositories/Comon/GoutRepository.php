<?php
/**
 * Created by PhpStorm.
 * User: Peaksource_Fakhri
 * Date: 12/16/2017
 * Time: 5:02 PM
 */

namespace App\Http\Repositories\Comon;


use App\Http\Models\Comon\Gout;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Espaces\espace_gout;
use App\Http\Models\Users\user_gout;

class GoutRepository implements IGoutRepository
{

    public function getAll()
    {
            return Gout::all();
    }

    public function getById($id)
    {
        return Gout::find($id);
    }

    public function getEspaceGouts($espace_id)
    {
        $gouts_id = espace_gout::where('espace_id', '=', $espace_id)->pluck('gout_id')->toArray();
        return Gout::whereIn('id', $gouts_id)->get();
    }

    public function getUserGouts($user_id)
    {
        $gouts_id = user_gout::where('user_id', '=', $user_id)->pluck('gout_id')->toArray();
        return Gout::whereIn('id', $gouts_id)->get();
    }

    public function getEspacesByGout($gouts_array)
    {
        $espaces_id = espace_gout::whereIn('gout_id', $gouts_array)->pluck('espace_id')->toArray();
        return Espace::whereIn('id', $espaces_id)->get();
    }

    public function addGoutToUser($gout)
    {
        $new = new user_gout();
            $new->user_id = $gout->user_id;
            $new->gout_id = $gout->gout_id;
        $new->save();
        return $new;
    }

    public function deleteUserGout($gout_id, $user_id)
    {
        user_gout::where('gout_id', '=', $gout_id)->where('user_id', '=', $user_id)->delete();
        return 'gout deleted';
    }

    public function addGoutToEspace($gout)
    {
        $new = new espace_gout();
            $new->espace_id = $gout->espace_id;
            $new->gout_id   = $gout->gout_id;
         $new->save();
         return $new;
    }

    public function deleteEspaceGout($gout_id, $espace_id)
    {
        espace_gout::where('gout_id', '=', $gout_id)->where('espace_id', '=', $espace_id)->delete();
        return 'gout deleted';
    }
}