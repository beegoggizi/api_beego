<?php

namespace App\Http\Repositories\Comon;


use App\Http\Models\Comon\Favorite;
use App\Http\Models\Comon\Feedback;
use App\Http\Models\Espaces\Espace;
use App\Http\Models\Users\User;
use App\Http\Models\Pictures\Espace_picture;

class FavoriteRepository implements IFavoriteRepository
{

    public function getUserFavorite($user)
    {
        $espacesId = Favorite::Where('user_id', '=', $user)->pluck('espace_id')->toArray();
        $espaces = Espace::whereIn('id', $espacesId)->get();

        foreach ($espaces as $espace)
            {
                $espace->favorite = Favorite::where('user_id', '=', $user)->where('espace_id', '=', $espace->id)->first();
                $espace->profile_picture = Espace_picture::where('espace_id', '=', $espace->id)->where('priority', '=', 0)->first()->path;
            }


        return ['status' => 'success', 'code' => '200', 'Espaces' => $espaces];

    }

    public function getEspaceFavorite($espace)
    {
        $usersId = Favorite::Where('espace_id', '=', $espace)->pluck('user_id')->toArray();
        $users = User::whereIn('id', $usersId)->get();

        foreach ($users as $user)
            $user->favorite = Favorite::where('user_id', '=', $user->id)->where('espace_id', '=', $espace)->first();

        return ['status' => 'success', 'code' => '200', 'Users' => $users];
    }

    public function deleteFavorite($user, $espace)
    {
        if($exist = Favorite::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first())
        {
            $exist->delete();
            return ['status' => 'success', 'code' => '200', 'msg' => 'favorite has been deleted'];
        }
        return ['status' => 'error', 'code' => '404', 'msg' => 'no favorite with the given couple (user_id, espace_id)'];

    }

    public function deleteFavoriteById($id)
    {
        if($exist = Favorite::find($id))
        {
            $exist->delete();
            return ['status' => 'success', 'code' => '200', 'msg' => 'favorite has been deleted'];
        }
        return ['status' => 'error', 'code' => '404', 'msg' => 'no favorite with the given id'];
    }

    public function addFavorite($user, $espace)
    {
        $exist = Favorite::where('user_id', '=', $user)->where('espace_id', '=', $espace)->first();
        if(!$exist)
        {
            Favorite::create([
                'user_id' => $user,
                'espace_id' => $espace,
            ]);
            return ['status' => 'success', 'code' => '200', 'msg' => 'favorite has been added'];
        }
        return ['status' => 'success', 'code' => '308', 'msg' => 'This user have already the indicated espace as favorite'];
    }
}