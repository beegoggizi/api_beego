<?php


namespace App\Http\Repositories\Comon;


interface IGoutRepository
{
    public function getAll();
    public function getById($id);
    public function getEspaceGouts($espace_id);
    public function getUserGouts($user_id);
    public function getEspacesByGout($gouts_array);

    public function addGoutToEspace($gout);
    public function deleteEspaceGout($gout_id, $espace_id);

    public function addGoutToUser($gout);
    public function deleteUserGout($gout_id, $user_id);
}