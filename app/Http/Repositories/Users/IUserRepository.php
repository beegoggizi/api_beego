<?php

namespace App\Http\Repositories\Users;

interface IUserRepository
{
    public function getUser($id);
    public function addUser($request);
    public function updateUser($request);
    public function updatePosition($longitude, $latitude, $user_id);
    public function createProgram($program);
    public function inviteToProgram($program_id, $invites);
    public function userExist($user);
    public function getProgramByUserId($user_id);
    public function programsImInvitedTo($user_id);
    public function getAll();
    public function searchUser($query);
    public function changePassword($request);
}