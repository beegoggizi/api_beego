<?php

namespace App\Http\Repositories\Users;

use App\Http\Models\Espaces\Espace;
use App\Http\Models\Pictures\UserPicture;
use App\Http\Models\Users\ProgramInvit;
use App\Http\Models\Users\Programme;
use App\Http\Models\Users\Role;
use App\Http\Models\Users\User;
use App\Http\Models\Users\UserMovement;
use App\Http\services;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class UserRepository implements IUserRepository
{

    public function getUser($id)
    {
        return User::find($id);
    }

    public function getAll()
    {
        $except = Role::pluck('user_id')->toArray();
        return User:: whereNotIn('id', $except)->simplePaginate(3);
    }

    public function addUser($request)
    {
        $new = new User();
        if(isset($request->name))       $new->name = $request->name;
            else return "The Name is required";
        if(isset($request->last_name))  $new->last_name= $request->last_name;
            else return "The Last Name is required";
        if(isset($request->email))      $new->email = $request->email;
            else return "The Email is required";
        if(isset($request->password))   $new->password = bcrypt($request->password);
            else return "The Password is required";

            $new->sexe = $request->sexe;
            $new->phone  = $request->phone;
            $new->gouvernorat =$request->gouvernorat;
            $new->birthday = $request->birthday;
       $new->save();

        if(isset($user->picture))
            if (strpos($new->picture, 'base64') !== false )
            {
                $path     = url('pic/users');
                $fileName = "user-".$new->id."-".time().".png";
                $fullUrl  = $path."/".$fileName;

                $picture = new UserPicture();
                    $picture->path           = $fullUrl;
                    $picture->file_name      = $fileName;
                    $picture->user_id        = $new->id;
                    $picture->created_at = Carbon::now();
                    Image::make($new->picture)->save('pic/users/'.$fileName);
                $picture->save();

                $new->picture_path = $fullUrl;
                $new->save();
            }
        $description = $new->name." ".$new->last_name."  has registred from the mobile app of beego";
        $service = new services();
        $service->addUserTrace($new->id, 'New registration', $description, "users", $new->id );

        return $new;
    }

    public function updateUser($user)
    {
        $update = User::find($user->id);
        if(isset($user->last_name))   $update->last_name = $user->last_name;
        if(isset($user->name))        $update->name = $user->name;
        if(isset($user->email))       $update->email = $user->email;
        if(isset($user->gouvernorat)) $update->gouvernorat = $user->gouvernorat;
        if(isset($user->address))     $update->address = $user->address;
        if(isset($user->sexe))        $update->sexe = $user->sexe;
        if(isset($user->phone))       $update->phone = $user->phone;
        if(isset($user->birthday))    $update->birthday = $user->birthday;

         if(isset($user->picture))
        {
            if (strpos($user->picture, 'base64') !== false )
            {
                $path     = url('pic/users');
                $fileName = "user-".$user->id."-".time().".png";
                $fullUrl  = $path."/".$fileName;

                $picture = new UserPicture();
                    $picture->path           = $fullUrl;
                    $picture->file_name      = $fileName;
                    $picture->user_id        = $user->id;
                    $picture->created_at = Carbon::now();
                    Image::make($user->picture)->save('pic/users/'.$fileName);
                $picture->save();
            }
        $update->picture_path = $fullUrl;
        }

        $service = new services();
         $description = $user->name." ".$user->last_name."  has updated his profile from the mobile app of beego";
        $service->addUserTrace($user->id, 'User update', $description, "users", $user->id );
        $service->UserHistory($user->id, 'Vous avez mis à jour votre profil');


        if($update->save())
            return $update;
        else return 'update fail';
    }

    public function updatePosition($longitude, $latitude, $user_id)
    {
        $user = User::find($user_id);
            $user->longitude = $longitude;
            $user->latitude  = $latitude;
        $user->save();

        $trace = new UserMovement();
            $trace->longitude = $longitude;
            $trace->latitude = $latitude;
            $trace->user_id = $user_id;
         $trace->save();
        return $user;
    }

    public function createProgram($program)
    {
        $new = new Programme();
            $new->user_id = $program->user_id;
            $new->espace_id = $program->espace_id;
            $new->date_time = $program->date_time;
        $new->save();

        if(isset($program->invites))
        {
            $trace = new services();
            $program = Programme::find($new->id);
            $espace_name = Espace::find($program->espace_id)->name;
            $user   = User::find($program->user_id);
            $user_name = $user->name.' '.$user->last_name;
            $invites = json_decode($program->invites);
            foreach ($invites as $invite)
            {
                $trace->UserHistory($invite, $user_name.' Vous a invité à son programme dans '.$espace_name.' qui aura lieu le '.$program->date_time);
                $invitations = new ProgramInvit();
                    $invitations->program_id = $new->id;
                    $invitations->invited = $invite;
                $invitations->save();
            }
        }

        return $new;
    }

    public function inviteToProgram($program_id, $invites)
    {
        $i = 0;
        $invites = json_decode($invites);
        $trace = new services();
        $program = Programme::find($program_id);
        $espace_name = Espace::find($program->espace_id)->name;
        $user   = User::find($program->user_id);
        $user_name = $user->name.' '.$user->last_name;
        foreach ($invites as $invite)
        {
            if(!$this->isInvited($program_id, $invite) && $this->userExist($invite))
            {
                $invitations = new ProgramInvit();
                    $invitations->program_id = $program_id;
                    $invitations->invited = $invite;
                $invitations->save();

                $trace->UserHistory($invite, $user_name.' Vous a invité à son programme dans '.$espace_name.' qui aura lieu le '.$program->date_time);
               // $trace->pushNotification($user_name.' Vous invite à son programme dans '.$espace_name.' qui aura lieu le '.$program->date_time, $invite);
                $i++;
            }

        }

        return $i." users are invited, the rest are already invited or does not exist";
    }

    public function getProgramByUserId($user_id)
    {
        $organizer = User::find($user_id);
        $programs = Programme::where('user_id', '=', $user_id)->orderBy('date_time', 'DESC')->get();
        foreach ($programs as $program)
        {
            $invited_ids = ProgramInvit::where('program_id', '=', $program->id)->pluck('invited')->toArray();
            $program->invited_users = User::whereIn('id', $invited_ids)->get();
        }
        $programs->organizer = $organizer;
        return $programs;
    }

    public function programsImInvitedTo($user_id)
    {
        $program_ids = ProgramInvit::where('invited', '=', $user_id)->pluck('program_id')->toArray();
        $programs = Programme::whereIn('id', $program_ids)->get();
        foreach ($programs as $program)
        {
            $organizer = User::find($program->user_id);
            $invited_ids = ProgramInvit::where('program_id', '=', $program->id)->pluck('invited')->toArray();
            $program->invited_users = User::whereIn('id', $invited_ids)->get();
            $program->organizer = $organizer;
        }
        return $programs;

    }


    public function isInvited($program_id, $user_id)
    {
        if($exist = ProgramInvit::where('program_id', '=', $program_id)->where('invited', '=', $user_id)->first())
            return true;
        else return false;
    }

    public function userExist($user)
    {
        if($exist = User::where('id', '=', $user)->first())
            return true;
        else return false;
    }

    public function searchUser($query)
    {
        return User::where('name', 'like', '%'.$query.'%')->orWhere('last_name', 'like', '%'.$query.'%')->get();
    }

    public function changePassword($request)
    {
        if($user = User::find($request->id))
        {
            $oldPass = $user->password;
            if($oldPass == bcrypt($request->oldPass))
            {
                $user->password = bcrypt($request->newPass);
                $user->save();
            }else return ["status" => "error", "code" => "302", "msg" => "the old password does not match"];

            return ["status" => "success", "code" => "200", "msg" => "Password updated"];

        }else return ["status" => "error", "code" => "404", "msg" => "User not found"];
    }
}