<?php

namespace App\Providers;

use App\Http\Repositories\Checks\CheckRepository;
use App\Http\Repositories\Checks\ICheckRepository;
use App\Http\Repositories\Comon\EloquentFollowRepository;
use App\Http\Repositories\Comon\FavoriteRepository;
use App\Http\Repositories\Comon\FeedbackRepository;
use App\Http\Repositories\Comon\GoutRepository;
use App\Http\Repositories\Comon\IEloquentFollowRepository;
use App\Http\Repositories\Comon\IFavoriteRepository;
use App\Http\Repositories\Comon\IFeedbackRepository;
use App\Http\Repositories\Comon\IGoutRepository;
use App\Http\Repositories\Espaces\EspaceRepository;
use App\Http\Repositories\Espaces\EventRepository;
use App\Http\Repositories\Espaces\IEspaceRepository;
use App\Http\Repositories\Espaces\IEventRepository;
use App\Http\Repositories\Espaces\IOfferRepository;
use App\Http\Repositories\Espaces\IPostRepository;
use App\Http\Repositories\Espaces\ISpecialityRepository;
use App\Http\Repositories\Espaces\OfferRepository;
use App\Http\Repositories\Espaces\PostRepository;
use App\Http\Repositories\Espaces\SpecialityRepository;
use App\Http\Repositories\Notifications\EloquentNotificationRepository;
use App\Http\Repositories\Notifications\IEloquentNotificationRepository;
use App\Http\Repositories\Users\IUserRepository;
use App\Http\Repositories\Users\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IEloquentFollowRepository::class,
            EloquentFollowRepository::class);

        $this->app->bind(IEloquentNotificationRepository::class,
            EloquentNotificationRepository::class);

        $this->app->bind(IEspaceRepository::class,
            EspaceRepository::class);

        $this->app->bind(ICheckRepository::class,
            CheckRepository::class);

        $this->app->bind(IFeedbackRepository::class,
                    FeedbackRepository::class);

        $this->app->bind(IFavoriteRepository::class,
                            FavoriteRepository::class);

        $this->app->bind(IUserRepository::class,
                            UserRepository::class);

        $this->app->bind(IEventRepository::class,
                            EventRepository::class);

        $this->app->bind(IPostRepository::class,
                            PostRepository::class);

        $this->app->bind(IGoutRepository::class,
                                    GoutRepository::class);

        $this->app->bind(ISpecialityRepository::class, SpecialityRepository::class);
        $this->app->bind(IOfferRepository::class, OfferRepository::class);
    }
}
