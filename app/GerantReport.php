<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GerantReport extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Http\Models\Users\User', 'user_id');
    }
}
