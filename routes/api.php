<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/signin', 'AuthController@authenticate');
Route::post('user/add', 'Api\ClientController@addUser'); // name, last_name, email, password, sexe (0 -> man, 1 -> fml), phone, gouvernorat

/**************** Auth api tokens and logins ******************/
Route::group(['middleware' => '\App\Http\Middleware\RefreshToken'], function () {

    // Protected routes

    /****************** Follow System *******************************/
    Route::get('{user}/follow/{espace}', 'Api\FollowController@follow');
    Route::get('{user}/unfollow/{espace}', 'Api\FollowController@unfollow');
    Route::get('followers/{espace}', 'Api\FollowController@followers');
    Route::get('follows/{user}', 'Api\FollowController@follows');
    Route::get('follows/accept/{id}', 'Api\FollowController@accept'); //Please note, the id is the id of the raw
    Route::get('follows/wait/{id}', 'Api\FollowController@wait');
    Route::get('follows/refuse/{id}', 'Api\FollowController@refuse');

    Route::get('follows/myWaitingList/{user_id}', 'Api\FollowController@myWaitingList'); //liste d'invitations
    Route::get('follows/myRequests/{user_id}', 'Api\FollowController@myRequests');


    Route::get('{follower_id}/userFollow/{followed_id}', 'Api\FollowController@userFollow'); //invitation the follower_id is the user that sent the invitation
    Route::get('{follower_id}/userUnfollow/{followed_id}', 'Api\FollowController@userUnfollow');
    Route::get('userFollowers/{user_id}', 'Api\FollowController@getUserFollowers');
    Route::get('followers/search/{espace_id}/{query}', 'Api\FollowController@search');

    /***************** Follow : Suggestions system ****************/
    // Gerant Apis
    Route::get('follows/suggestions/{espace_id}', 'Api\FollowController@getUsersSuggestions');
    Route::get('follows/sendRequest/{espace_id}/{user_id}', 'Api\FollowController@sendRequestToUser');
    Route::get('follows/getEspaceRequests/{espace_id}', 'Api\FollowController@getEspaceRequests');
    // Client Apis
    Route::get('follows/getUserRequests/{user_id}', 'Api\FollowController@getUserRequests');
    Route::get('follows/acceptGerantRequest/{user_id}/{espace_id}', 'Api\FollowController@acceptGerantRequest');
    Route::get('follows/refuseGerantRequest/{user_id}/{espace_id}', 'Api\FollowController@refuseGerantRequest');


    /**************** Notifications ************************/
    Route::get('user/notifications/{user}', 'Api\NotificationController@getUserNotifications');
    Route::get('espace/notifications/{espace}', 'Api\NotificationController@getEspaceNotifications');


    // TO DO post notification by Beego but i don't care it's not my job x)

    /**************** Feedbacks ****************************/
    Route::get('Feedbacks/espace/{espace}', 'Api\FeedbackController@getEspaceFeedbacks'); //espace mean the espace_id ofc.. same for user...
    Route::get('Feedbacks/user/{user}', 'Api\FeedbackController@getUserFeedbacks');
    Route::get('Feedbacks/delete/{user}/{espace}', 'Api\FeedbackController@deleteFeedback'); //You may use this if you want to use the couple espace_id n user_id
    Route::get('Feedbacks/delete/{id}', 'Api\FeedbackController@deleteFeedbackById');//You may use this if you want to track the feedback id
    Route::get('feedback/{id}/{like}', 'Api\FeedbackController@like');

    Route::post('Feedbacks/addNew', 'Api\FeedbackController@addFeedback'); // required : user_id, espace_id, comment, note (send an empty string if null)
    Route::post('Feedbacks/update', 'Api\FeedbackController@updateFeedback'); // required : user_id, espace_id, comment, note (send all of them even if the user didn't change )
    Route::post('Feedbacks/updateById', 'Api\FeedbackController@updateFeedback'); // required : id, comment, note (send all of them even if the user didn't change ) The id is the id of the espace

    /*************** Favorites *************************/
    Route::get('Favorites/espace/{espace_id}', 'Api\FavoritesController@getEspaceFavorites'); //get the list of the users making that espace as favorite (each user contain the favorite itself so you can extract the id)
    Route::get('Favorites/user/{user_id}', 'Api\FavoritesController@getUserFavorites'); //get the list of espaces that the indicated user_id maked them as favorite
    Route::get('Favorites/delete/{user_id}/{espace_id}', 'Api\FavoritesController@delete'); //You may use this if you want to use the couple espace_id n user_id
    Route::get('Favorites/delete/{id}', 'Api\FavoritesController@deleteById');//You may use this if you want to track the feedback id
    Route::get('Favorites/create/{user_id}/{espace_id}', 'Api\FavoritesController@create');


    /**************** Espace ************************/
    Route::get('espace/byId/{espace}', 'Api\EspaceController@getEspaceById');
    Route::get('espaces/getAll', 'Api\EspaceController@getAllEspaces');
    Route::get('espaces/all/{dist}/{user_lng}/{user_lat}', 'Api\EspaceController@getEspaces');
    Route::post('espace', 'Api\EspaceController@updateEspace'); //messing the picture part :p it will be available tomorrow morning
    Route::post('espace/forceState', 'Api\EspaceController@forceState'); // send the 'espace_id' and 'color' and it will force that color, you can get them under the espace object->config
    Route::get('espace/unforceState/{espace_id}', 'Api\EspaceController@unforceState'); //send the 'espace_id' , this function will reset the 'isForced' at 0 and delete the 'forcedColor'
    Route::get('espace/search/{name}', 'Api\EspaceController@search');
    Route::get('espace/suggestions/all', 'Api\EspaceController@getSuggestions');
    Route::get('espace/suggestions/{espace_id}', 'Api\EspaceController@getSuggestionByEspace');
    //suggestion id => nbre d'espaces eli 3andhom suggestion heki
    Route::get('espace/countSuggestions/{suggestion_id}', 'Api\EspaceController@CountSuggestionEspaces');
    //suggestion id => liste d'espaces ayant cette suggestion
    Route::get('espace/getFromSuggestion/{suggestion_id}', 'Api\EspaceController@getSuggestionEspace');
    Route::get('espace/promotions/{espace_id}', 'Api\EspaceController@getOffersByEspace');
    Route::get('espace/getEspaceNews/{espace_id}', 'Api\EspaceController@getEspaceNews');
    Route::get('espace/getFollowersCount/{espace_id}', 'Api\EspaceController@getFollowersCount');
    Route::get('espace/activities/{espace_id}/{date}', 'Api\EspaceController@espaceActivities');
    Route::get('espace/getTime/{espace_id}', 'Api\EspaceController@getHoraire');
    Route::post('espace/changeTime', 'Api\EspaceController@changeHoraire');


    /************** Users **************************/
    Route::get('user/getById/{id}', 'Api\ClientController@getUser'); //send the id and you will get the user
    Route::get('user/all', 'Api\ClientController@getAll');
    Route::post('user', 'Api\ClientController@updateUser'); // done
    Route::post('user/position', 'Api\ClientController@updatePosition'); // longitude, latitude, user_id it return the user
    Route::get('user/search/{query}', 'Api\ClientController@search');
    Route::post('user/changePassword', 'Api\ClientController@changePassword'); // send : oldPass, newPass and id (the id of the user)


    /************* User Programs *****************/
    Route::post('user/createProgram', 'Api\ClientController@createProgram');
    Route::post('user/inviteToProgram', 'Api\ClientController@inviteToProgram');
    Route::get('user/getUserPrograms/{user_id}', 'Api\ClientController@getUserPrograms');
    Route::get('user/programsImInvitedTo/{user_id}', 'Api\ClientController@programsImInvitedTo');

    /************** Events **************************/
    Route::post('event/add', 'Api\EventsController@new');
    Route::post('event/update', 'Api\EventsController@update');
    Route::get('event/delete/{id}', 'Api\EventsController@delete');
    Route::get('event/espace/{espace_id}', 'Api\EventsController@getEspaceEvents');
    Route::get('event/all', 'Api\EventsController@getAll');
    Route::get('event/{user_id}/isInterrestedIn/{event_id}', 'Api\EventsController@userInterrested');

    /**************** Posts *************************/
    Route::post('post/add', 'Api\PostsController@new');
    Route::post('post/update', 'Api\PostsController@update');
    Route::get('post/delete/{id}', 'Api\PostsController@delete');
    Route::get('post/espace/{espace_id}', 'Api\PostsController@getEspacePosts');
    Route::get('post/byUser/{user_id}', 'Api\PostsController@postsByUser'); //provide the posts of the espaces that the user follow or mark them as favorite,if they are less then 10 it add the 20 last posts

    /************** Gouts **************************/
    Route::get('gouts/all', 'Api\GoutsController@getAll'); //return all the gouts that exists from the table gouts
    Route::get('gouts/{id}', 'Api\GoutsController@getGoutById'); //return a specific gout by the gout id, don't know why maybe you need the description/name of the gout
    Route::get('gouts/getEspaceGouts/{espace_id}', 'Api\GoutsController@getEspaceGouts'); //return the gouts of the given espace_id
    Route::get('gouts/getUserGouts/{user_id}', 'Api\GoutsController@getUserGouts'); //return the gouts of the given user id (if null use the get all so the user can initialize his gouts)
    Route::post('gouts/getEspacesFromGouts', 'Api\GoutsController@getEspacesFromGouts'); // Accept an array of gouts_id example: gouts = [1, 5, 7] and it will return the espaces which have the indicated gouts
    Route::post('gouts/addGoutToUser', 'Api\GoutsController@addGoutToUser'); // formbody : gout_id / user_id => it will assign the given gout_id to the given  user_id
    Route::post('gouts/addGoutToEspace', 'Api\GoutsController@addGoutToEspace'); // formbody: gout_id / espace_id => it will assign the given gout_id to the given espace_id
    Route::get('gouts/deleteUserGout/{gout_id}/{user_id}', 'Api\GoutsController@deleteUserGout'); //send the gout_id and the user_id, the function will check if it exist and delete
    Route::get('gouts/deleteEspaceGout/{gout_id}/{espace_id}', 'Api\GoutsController@deleteEspaceGout'); //send the gout_id and the espace_id and it will check and delete

    /************* Specialities *******************/
    Route::get('specialities/get/{id}', 'Api\SpecialitiesController@get'); // send the espace id and it will return it specialities
    Route::get('specialities/delete/{id}', 'Api\SpecialitiesController@delete'); // send the speciality id and it will dlete it
    Route::post('specialities/add', 'Api\SpecialitiesController@add'); //send name, espace_id, description (required)
    Route::post('specialities/update', 'Api\SpecialitiesController@update'); //send the new name, espace_id, description (required)
    Route::get('specialities/for_user/{user_id}', 'Api\SpecialitiesController@getUserSpecialities'); // will return the specialies of the spaces that the given user_id follow or marked as favorite

    /************* offers *******************/
    Route::get('offers/get/{id}', 'Api\OfferController@get'); // send the espace id and it will return it offers
    Route::get('offers/delete/{id}', 'Api\OfferController@delete'); // send the offers id and it will dlete it
    Route::post('offers/add', 'Api\OfferController@add'); //send name, espace_id, description (required)
    Route::post('offers/update', 'Api\OfferController@update'); //send the new name, espace_id, description (required)

    /***************** Specialement pour vous ****************/
    Route::get('spv/delete/{user_id}/{espace_id}', 'Api\SpecialementPourVousController@delete');
    Route::get('spv/getByEspaceId/{espace_id}', 'Api\SpecialementPourVousController@getByEspaceId');
    Route::get('spv/getByUserId/{user_id}', 'Api\SpecialementPourVousController@getByUserId');
    //Required : espace_id, user_id, description | optional : image (please note : the picture must be sent in base64 format)
    Route::post('spv/update', 'Api\SpecialementPourVousController@update');
    //Required : id (the id of the row) | optional : espace_id, user_id, description, image (base64)
    Route::post('spv/add', 'Api\SpecialementPourVousController@add');

    /************** More infos ****************/
    Route::get('mi/delete/{id}', 'Api\MoreInfosController@delete');
    Route::get('mi/get/{espace_id}', 'Api\MoreInfosController@get');
    Route::post('mi/add', 'Api\MoreInfosController@add');
    Route::post('mi/update', 'Api\MoreInfosController@update');

    /************* service ***************/
    Route::get('service/getAll', 'Api\ServicesController@getAll'); //return all the services
    Route::get('service/byespaceid/{espace_id}', 'Api\ServicesController@byespaceid'); //return the services of the indicated espace_id
    Route::get('service/byserviceid/{service_id}', 'Api\ServicesController@byserviceid'); //return the service of the given id

    /************** Share Location *************/
    // keep in your mind that the id of the returned row is usefull for accepting/rejecting...
    Route::get('shareLocation/invite/{invited_user_id}/{user_id}', 'Api\ShareLocationController@invite');
    // the share_id is the id returned when the user send an invitation (the id of the table share_locations )
    Route::get('shareLocation/accept/{share_id}/{long}/{lat}', 'Api\ShareLocationController@accept');
    Route::get('shareLocation/reject/{share_id}', 'Api\ShareLocationController@reject');
    //will return the location of the user_id that he shared his position with the {user_Im_sharing_With}
    Route::get('shareLocation/getPosition/{user_id}/{user_Im_sharing_With}', 'Api\ShareLocationController@getPosition');
    //will return the invitations that i sent for people to share with me their position (the pending invitations)
    Route::get('shareLocation/myInvits/{user_id}', 'Api\ShareLocationController@myInvits');
    //will return the list of people who share their positions with me and the infos of the share
    Route::get('shareLocation/peopleSharingWithMe/{user_id}', 'Api\ShareLocationController@peopleSharingWithMe');
    //will return the people that i'm sharing with  myShares
    Route::get('shareLocation/myShares/{user_id}', 'Api\ShareLocationController@myShares');


    /*********** Categories ******************/
    Route::get('categories/{espace_id}', 'Api\CategoriesController@getEspaceCategories'); //send the espace_id and you will get it categories (sous categories)

    //For tests in category, with those api you can add/delete/update categories and sous_categories
    Route::get('sousCategory/addToEspace/{espace_id}/{sous_category_id}', 'Api\CategoriesController@addSousCategoryToEspace'); // it will add a sous_category for a specified espace

    Route::get('categories/delete/{category_id}', 'Api\CategoriesController@delete'); //send the category id and it will be deleted
    Route::post('categories/update', 'Api\CategoriesController@update'); // send : name, description, id (the id of the category) (description is optional)
    Route::post('categories/add', 'Api\CategoriesController@add'); // send : name, description (optional)
    Route::get('sousCategories/of/{category_id}', 'Api\CategoriesController@getSousCategory');


    Route::post('sousCategory/add', 'Api\CategoriesController@addSousCategory');
    Route::get('sousCategory/delete/{sous_category_id}', 'Api\CategoriesController@deleteSousCategory');

    /********** Reports System *************/
    Route::post('Reports/updateReport', 'Api\ReportsController@updateReport');
    Route::post('Reports/reportUser', 'Api\ReportsController@reportUser');
    Route::get('Reports/getEspaceReports/{espace_id}', 'Api\ReportsController@getEspaceReports');
    Route::get('Reports/deleteReport/{report_id}', 'Api\ReportsController@deleteReport');

    /*********** History ***************/
    Route::get('History/getUserHistory/{user}', 'Api\ClientController@getUserHistory'); //send the user_id and it will return his history
    Route::get('History/deleteUserHistory/{user}', 'Api\ClientController@deleteHistory'); //send the user_id and it will delete his history
});
